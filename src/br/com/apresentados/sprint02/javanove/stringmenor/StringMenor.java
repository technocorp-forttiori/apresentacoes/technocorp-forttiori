package br.com.apresentados.sprint02.javanove.stringmenor;

import java.time.Duration;
import java.time.Instant;
import java.util.Random;
import java.util.stream.Collectors;

public class StringMenor {
    public synchronized static void main(String[] args) throws InterruptedException {
        Instant inicio = Instant.now();
        Thread.sleep(1000);

        new Random().ints()
                .limit(10_100_100)
                .boxed()
                .map(i -> i.toString())
                .collect(Collectors.joining());
        Thread.sleep(5000);
        Instant fim = Instant.now();
        System.out.println(Duration.between(inicio, fim));
    }
}
