package br.com.apresentados.sprint02.javanove.staticfactorymethods;

import java.util.*;

public class Exemplos {
    public static void main(String[] args) {
        // Nomes com Significado
        Optional<Integer> empty = Optional.empty();
        Optional<Integer> cemOpt = Optional.of(100); // não tem como saber.
        Optional<Integer> vazio = Optional.ofNullable(null);

        // Flexibilidade no retorno
        List<Object> list = new ArrayList<>();
        List<Object> novaLista = Collections.unmodifiableList(list);
        // novalista.add(1); // Lança uma exceção

        // Controle das Instâncias
        Integer cem = Integer.valueOf(100);
        Integer duz = Integer.valueOf(200);
        Integer tre = Integer.valueOf(300);

        // Novidades - List - Map - Set
        // Como era Antes ---
        List<Integer> list2 = Arrays.asList(cem,duz,tre);

        //Formato Novo Java 9
        // List.of tem vários elementos.
        List<Integer> of4 = List.of(1,2,3);
        List<Object> of = List.of(cem,100,tre); // Posso criar elementos na lista
        List<Integer> of2 = List.of(cem);
        List<Integer> of3 = List.of(1,2,3);
        // Imutável
        // of4.add(1);

//        Map.of(k1,v1);
//        Set.of(e1,e2);

        Integer i1 = new Integer(null);
        Integer i2 = Integer.valueOf(100);
        int i3;

    }
}
