package br.com.apresentados.sprint02.javaoito.streams;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;

public class Java8Streams {
    public static void main(String[] args) {
        List<Integer> lista = Arrays.asList(1,5,8,9,9,1,4,7,6,6,9,9);
        List<Integer> listaParaUsar = lista;
        // JAVA 8 STREAMS

        // JAVA 5
//        for (Integer integer : lista){
//            System.out.println(integer);
//        }
        // JAVA 8
        listaParaUsar.stream()
                //-- operações intermediárias.
                .filter(e -> e % 2 == 0) // faz um filtro dependendo do que for necessário no projeto, neste caso traz somente numeros pares.
                .skip(2) // pula os elementos de acordo com o parâmetro
                .limit(5) // limita quantos elementos devem ser lidos na stream
                .distinct() // ignora os elementos repetidos - utiliza equals e hashcode
                .forEach(System.out::println); // Operação Final do Stream.
        listaParaUsar.stream()
                .map(e -> e * 2) // a lista original não é modificada, altera apenas no dado que passa na stream.
                .forEach(System.out::println);

        long count = lista.stream()
                .filter(e -> e % 2 == 0)
                .count();
        System.out.println(count);

        Optional<Integer> comparator = lista.stream()
                .min(Comparator.naturalOrder()); // min ou max
        System.out.println("O menor número da lista é: " + comparator.get());
    }
}
