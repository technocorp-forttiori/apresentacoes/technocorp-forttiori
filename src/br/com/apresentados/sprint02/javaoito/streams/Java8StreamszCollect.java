package br.com.apresentados.sprint02.javaoito.streams;

import java.util.*;
import java.util.stream.Collectors;

public class Java8StreamszCollect {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5, 6);

        // fornecedor(supply) - acumulação - combinação
        List<Integer> collect = list.stream()
                .collect(
                        () -> new ArrayList<>(), // Instancia do que quero usar para armazenar os resultados
                        (l, e) -> l.add(e), // como vou fazer o armazenamento dentro
                        (l1, l2) -> l1.addAll(l2) // como é feita a combinação das threads que estão tratando
                );
        System.out.println(collect);
        System.out.println(list);

        // toList / toSet
        List<Integer> collect2 = list.stream()
                .collect(Collectors.toList());
        System.out.println(collect2);

        String join = list.stream() // Serve para juntar strings
                .map(n -> n.toString())
                //.collect(Collectors.joining(""));
                .collect(Collectors.joining("a"));
        System.out.println(join);

        //avaring -- (média dos elementos)
        Double collectAvaring = list.stream()
                .collect(Collectors.averagingInt(n -> n.intValue()));
        System.out.println(collectAvaring);

        // summing - soma
        Integer soma = list.stream()
                .collect(Collectors.summingInt(n -> n.intValue()));
        System.out.println(soma);

        // summarizing - várias operações em uma chamada só.
        IntSummaryStatistics stats = list.stream()
                .collect(Collectors.summarizingInt(n -> n.intValue()));
        System.out.println(stats.getAverage());
        System.out.println(stats.getMax());
        System.out.println(stats.getMin());
        System.out.println(stats.getCount());
        System.out.println(stats.getSum());

        // counting
        Long itens = list.stream()
                .filter((n) -> n % 2 == 0)
                .collect(Collectors.counting());
        System.out.println(itens);

        // max/min
        list.stream()
                .filter((n) -> n % 2 == 0)
                .collect(Collectors.maxBy(Comparator.naturalOrder()))
                .ifPresent(System.out::println);

        // groupingBy
        Map<Integer, List<Integer>> groupBy = list.stream()
                .collect(Collectors.groupingBy((n) -> n % 3));
        System.out.println(groupBy);

        // partitioningBy -- BOOLEAN reotrno
        Map<Boolean, List<Integer>> partgroupBy = list.stream()
                .collect(Collectors.partitioningBy((n) -> n % 2 == 0));
        System.out.println(partgroupBy);

        // toMap
        Map<Integer, Integer> toMap = list.stream()
                .collect(Collectors.toMap(n -> n, n -> n * 2));
        System.out.println(toMap);
        // toMap - Potencia
        Map<Integer, Double> toMap2 = list.stream()
                .collect(Collectors.toMap(
                        n -> n,
                        n -> Math.pow(n.doubleValue(),5)
                        ));
        System.out.println(toMap2);
    }

}
