package br.com.apresentados.sprint02.javaoito.streams;

import java.io.*;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.regex.Pattern;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Java8StreamzFormaDeCriar {
    public static void main(String[] args) throws IOException {
        // Collection
        List<Integer> list = Arrays.asList(1,2,3,4);
        list.stream()
                .forEach(System.out::println);

        //Arrays - Passando array para stream
        Integer[] intArray = new Integer[] {1,2,3,4};
        Arrays.stream(intArray)
                .forEach(System.out::println);

        // Stream.of - criar stream de qualquer tipo de objeto, qualquer valor
        Stream.of("Teste", 1, "Teste")
                .forEach(System.out::println);

        // IntStream.range -- range não inclui o numero exclusivo, rangeClosed inclui
        IntStream.rangeClosed(0,3)
        .forEach(System.out::println);

        // Stream.iterate -- tem que por limite, ou loop infinito. Recebe um Seed(Semente para valor inicial)
        Stream.iterate(5,n -> n * 2)
                .limit(3)
                .forEach(System.out::println);

        // BufferedReader - liner
        File file = new File("streams.txt");
        FileReader in = new FileReader(file);
        try (BufferedReader br = new BufferedReader(in)) {
            br.lines().forEach(System.out::println);
        } catch (IOException e) {
            e.printStackTrace();
        }

        // Files - Retorna os arquivos
//        Path p = Paths.get("");
//        Files.list(p).forEach(System.out::println);

        // Random - retorna numeros inteiros aleatórios
        new Random().ints()
                .limit(5)
                .reduce((e1,e2) -> e1 + e2)
                .ifPresent(System.out::println);

        // Pattern - Trabalha com expressões regulares, Rejects.
        String s = "Testando,Pattern,Teste Testando Pattern Teste";
        Pattern compile = Pattern.compile(" ");
        compile.splitAsStream(s).forEach(System.out::println);
    }
}
