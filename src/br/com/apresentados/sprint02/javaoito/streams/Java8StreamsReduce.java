package br.com.apresentados.sprint02.javaoito.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.OptionalDouble;
import java.util.stream.DoubleStream;

public class Java8StreamsReduce {
    public static void main(String[] args) {

        List<Integer> listSoma = Arrays.asList(1,2,3,4,5,6);
        //soma
        Optional<Integer> reduce = listSoma.stream()
                .reduce((n1, n2) -> n1 + n2);
        System.out.println(reduce.get());

        // multiplicação
        List<Integer> listMulti = Arrays.asList(1,2,3,4,5,6);

        Optional<Integer> reduceMulti = listMulti.stream()
                .reduce((n1,n2) -> n1 * n2);
        System.out.println(reduceMulti.get());

        // Concatenação
        String s = "Teste para concatenação";
        String[] split = s.split(" ");
        List<String> listStr = Arrays.asList(split);

        Optional<String> concatenacao = listStr.stream()
                .reduce((s1, s2) -> s1.concat(s2));
        System.out.println(concatenacao.get());

        // subtracao -- não é uma função associativa. (não é de boa conduta fazer funções não associativas em streams).
        List<Integer> listSub = Arrays.asList(1,2,3,4,5,6);
        Optional<Integer> subtracao = listSub.stream()
                //.parallel()
                .reduce((n1, n2) -> n1 - n2);
        System.out.println(subtracao.get());

        // valor com identidade - SOMA (0) - Multiplicação (1) - concatenação ("string vazia")
        List<Integer> listSomaIdentidade = Arrays.asList(1,2,3,4,5,6);

        Integer reduceIdent = listSomaIdentidade.stream()
                .reduce(0,(n1, n2) -> n1 + n2);
        System.out.println(reduceIdent);

        // reduce - menor valor
        OptionalDouble  reduceD = DoubleStream.of(1.2,2.9,6.7)
                .reduce((d1,d2) -> Math.min(d1,d2));
        System.out.println(reduceD);

        // reduce - combinação
        Integer combinacao = listSomaIdentidade.stream()
                .reduce(0,(n1, n2) -> n1 + n2,(n1, n2) -> n1 + n2);
        System.out.println(combinacao);

        // reduce - map + combiner
        Optional<String> concatmap = listSomaIdentidade.stream()
                .map(n1 -> n1.toString())
                .reduce((n1,n2) -> n1.concat(n2));
        System.out.println(concatmap);
    }
}
