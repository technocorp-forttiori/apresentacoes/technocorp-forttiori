package br.com.apresentados.sprint02.javaoito.streams;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Java8StreamsCollectors {
    public static void main(String[] args) {
        List<Integer> lista = Arrays.asList(1,2,3,4,5,6,7,8,9);

        List<Integer> novaLista = lista.stream()
                .filter(e -> e % 2 == 0)
                .collect(Collectors.toList());
        System.out.println(novaLista);

        Map<Boolean, List<Integer>> mapa = lista.stream()
                .map(e -> e *3)
                .collect(Collectors.groupingBy(e -> e % 2 == 0));
        System.out.println(mapa);

        Map<Integer, List<Integer>> mapa2 = lista.stream()
                .collect(Collectors.groupingBy(e -> e % 3)); // agrupando pelo resto da divisão por 3
        System.out.println(mapa2);

        String colecao = lista.stream()
                .map(String::valueOf)
                .collect(Collectors.joining(" ")); // Junta os elementos, mas só trabalha com string
                //.collect(Collectors.joining()); // Junta os elementos, mas só trabalha com string

        System.out.println(colecao);
    }
}
