package br.com.apresentados.sprint02.javaoito.apidatas;

import java.time.Duration;
import java.time.LocalTime;

public class Java8ApiDataDuration {
    public static void main(String[] args) {

        Duration ofDays = Duration.ofDays(1);
        System.out.println(ofDays);

        Duration ofHours = Duration.ofHours(1);
        System.out.println(ofHours);

        Duration ofMinutes = Duration.ofMinutes(500);
        System.out.println(ofMinutes);

        Duration ofSeconds = Duration.ofSeconds(10,5000);
        System.out.println(ofSeconds);

        LocalTime ltNow = LocalTime.now();
        LocalTime lt = LocalTime.of(11,10,1);
        Duration duration = Duration.between(ltNow,lt);
        System.out.println(duration);

        //modificando bt

        Duration plusMillis = duration.plusMillis(500);
        System.out.println(plusMillis);
    }
}
