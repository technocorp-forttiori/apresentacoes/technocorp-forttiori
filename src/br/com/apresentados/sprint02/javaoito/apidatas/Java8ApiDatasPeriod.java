package br.com.apresentados.sprint02.javaoito.apidatas;

/* JAVA 8 API DATA E HORA
    Peorid e Duration
 */

import java.time.LocalDate;
import java.time.Month;
import java.time.Period;

public class Java8ApiDatasPeriod {
    public static void main(String[] args) {
        Period of = Period.of(1,5,3);
        System.out.println(of);

        Period ofDays = Period.ofDays(5);
        System.out.println("Dia: " + ofDays);

        Period ofMonth = Period.ofMonths(5);
        System.out.println("Mês: " + ofMonth);

        Period ofWeeks = Period.ofWeeks(5);
        System.out.println("Armazena em dias: " + ofWeeks);

        Period ofYears = Period.ofYears(5);
        System.out.println("Ano: " + ofYears);

        // Calculo entre períodos diferentes.
        LocalDate ld = LocalDate.of(1990, Month.AUGUST,6);
        LocalDate ld2 = ld.plusWeeks(2).plusDays(1).plusMonths(1);
        Period between = Period.between(ld, ld2);
        System.out.println("O perído entre ld e ld2 é de: " + between);

        // adicionando dias entre o período calculado.
        Period between2 = between.plusDays(10);
        System.out.println(between2);

        // outra forma de calcular período
        Period until = ld.until(ld2);
        System.out.println("AAA " + until);

        //adicionado periodo em localdate
        LocalDate plus = ld.plus(ofYears);
        System.out.println("Adicionado 5 anos ao período: " + plus);

        //Calculando Nascimento
        LocalDate nascimento = LocalDate.of(1993,Month.DECEMBER,21);
        LocalDate hoje = LocalDate.now();
        Period periodo = Period.between(nascimento, hoje);
        System.out.println("O periodo entre meu nascimento e hoje é: " + periodo);
    }
}
