package br.com.apresentados.sprint02.javaoito.apidatas;

import java.time.*;

public class Java8ApiDatas {
    public static void main(String[] args) {

        // LocalDate - Representa uma Data
        LocalDate ldNow = LocalDate.now();
        System.out.println(ldNow);
        LocalDate ld = LocalDate.of(2021, Month.MAY,26);
        System.out.println(ld);
        // Manipulação -- Serve para todas as classes esses metodos plus.
        System.out.println(ld.plusWeeks(2));
        System.out.println(ld.plusDays(2));
        System.out.println(ld.plusYears(30));
        System.out.println(ld.plusDays(1).plusYears(1).plusMonths(4));

        // LocalTime - Representa uma hora: 12:00:00
        LocalTime ltNow = LocalTime.now();
        System.out.println(ltNow);
        LocalTime lt = LocalTime.of(10,9,01,100000);

        // LocalDateTime - Representa Data + Hora. 01/01/2000 12:00:00
        LocalDateTime ldtNow = LocalDateTime.now();
        System.out.println(ldtNow);
        LocalDateTime ldt = LocalDateTime.of(ld,lt);
        System.out.println(ldt);

        // Instant - Rpresenta um instante / momento, na linha do tempo. (milissegundos a partir de 01/01/1970 00:00:00)
        // Instant - Também representa 01/01/2000 12:00:00 GMT/UTC (não tem localidade)
        Instant iNow = Instant.now();
        System.out.println(iNow);
        Instant i = Instant.ofEpochMilli(5000000000L);
        System.out.println("Milisegundos " + i);
        // Exemplo de Instant que não funciona com manutenção, pois Instant é feito em milisegundos, e meses não pode ser medido com mili.
        // Instant plus = i.plus(1, ChronoUnit.MONTHS);

        // ZonedDateTime - LocalDateTime com TimeZone (fuso horário)
        // 06/08/1990 12:00:00 GMT-3 (America/BRASILIA)
        // A diferença entre Instant e ZonedDatTime, é que Instant não armazena informação do fuso horário.
        ZonedDateTime zdtNow = ZonedDateTime.now();
        System.out.println(zdtNow);
        ZonedDateTime zdt = ZonedDateTime.of(ldt,ZoneId.systemDefault());
        System.out.println(zdt);
    }
}
