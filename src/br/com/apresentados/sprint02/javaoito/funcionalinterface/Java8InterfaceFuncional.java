package br.com.apresentados.sprint02.javaoito.funcionalinterface;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.stream.Stream;

public class Java8InterfaceFuncional {
    public static void main(String[] args) {

        // Supplier - > Representa uma função que vai entregar alguma coisa, não recebe nenhum parâmetro.
        // Ex: Abaixo -- Um método fornecedor.
        Stream.generate(()-> new Random().nextInt()) // Supplier - Fornecedor
                .limit(5)
                .forEach((e) -> System.out.println(e)); // Consumer - Consumidor - Recebe elemento e não retorna nada
        List<Integer> list = Arrays.asList(1,2,3,4,5);
        list.stream()
                .filter(e -> e % 2 == 0) // Predicate - Teste de valores, comparação etc.
                .map(e -> e.doubleValue()) // Function - Recebe um valor e retorna outro valor.
                .reduce((e1, e2) -> e1 + e2) // BinaryOperator - Trabalham sempre com o mesmo tipo.
                .ifPresent(System.out::println); //Consumer sendo utilizado, faz algo.
    }
}
