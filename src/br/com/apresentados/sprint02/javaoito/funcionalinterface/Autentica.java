package br.com.apresentados.sprint02.javaoito.funcionalinterface;

@FunctionalInterface
public interface Autentica {

    public abstract boolean autenticaSenha(String senha);
    default void mover(){
        System.out.println("Teste");
    }
}
