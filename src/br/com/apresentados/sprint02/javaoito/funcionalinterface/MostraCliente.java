package br.com.apresentados.sprint02.javaoito.funcionalinterface;

import java.util.function.Consumer;

public class MostraCliente implements Consumer {
    public void accept(Cliente c) {
        System.out.println(c.getNome());
        System.out.println(c.getSenha());
    }

    @Override
    public void accept(Object o) {

    }

    @Override
    public Consumer andThen(Consumer after) {
        return Consumer.super.andThen(after);
    }
}
