package br.com.apresentados.sprint02.javaoito.funcionalinterface;

import java.util.Arrays;
import java.util.List;

public class Teste {
    public static void main(String[] args) {
        Cliente c1 = new Cliente("Matheus",true, "1234");
        Cliente c2 = new Cliente("Teste",true,"123");
        c1.autenticaSenha("123");
        c1.mover();

        List<Cliente> clientes = Arrays.asList(c1,c2);
        MostraCliente mc = new MostraCliente();
        clientes.forEach(mc);
    }
}
