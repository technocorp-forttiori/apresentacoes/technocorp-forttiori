package br.com.apresentados.sprint02.javaoito.generics;

import br.com.apresentados.sprint02.javaoito.generics.wildcards.*;

public class Programa {
    public static void main(String[] args) {
        SelecionadorAleatorio<Pessoa> sap = new SelecionadorAleatorio<>();

        Pessoa p1 = new Pessoa("Matheus", 27);
        Pessoa p2 = new Pessoa("Douglas", 25);
        Pessoa p3 = new Pessoa("Lucas", 20);

        sap.addElem(p1);
        sap.addElem(p2);
        sap.addElem(p3);

        Pessoa p = sap.sortear();

        System.out.println("O Sorteado foi: " + p.getNome());

        SelecionadorAleatorio<Integer> sai = new SelecionadorAleatorio<>();

        sai.addElem(1);
        sai.addElem(2);
        sai.addElem(5);

        System.out.println("O número sorteado foi: " + sai.sortear());

        SelecionadorAleatorio<Fundacao> saf = new SelecionadorAleatorio<>();
        saf.addElem( new Fundacao("Technocorp", 10,"4105645640",115101,"RS"));

        UnknowWildCard wildCardUnk = new UnknowWildCard();
        System.out.println("O Objeto com WildCard Unknow é: " + wildCardUnk.sortear( sap ));
        System.out.println("O Inteiro com WildCard Unknow é: " + wildCardUnk.sortear( sai ));


        //  WILDCARD EXTENDS

        SelecionadorAleatorio<PessoaJuridica> pessoaJuridica = new SelecionadorAleatorio<>();
        pessoaJuridica.addElem(new PessoaJuridica("Teste",10,"Teste"));
        pessoaJuridica.addElem(new PessoaJuridica("Teste2",10,"Teste2"));

        ExtendsWildCard spdj = new ExtendsWildCard();

        //spdj.sortear( sai); // Falha porque não é uma pessoa juridica ou filho dela.
        System.out.println("Fundação filho de pessoa Juridica: " + spdj.sortear(saf));
        System.out.println("Pessoa Juridica classe: "+ pessoaJuridica.sortear());


        // SUPER WILD CARD

        SuperWildCard spja = new SuperWildCard();
        System.out.println("Pessoa Jurídica: " + spja.sortear(pessoaJuridica));
        System.out.println("Pessoa normal mãe de pessoa juridica: " + spja.sortear(sap));
        //System.out.println(": " + spja.sortear(saf)); // não funciona pois não é ascendente de pessoa juridica

    }

}
