package br.com.apresentados.sprint02.javaoito.generics;

import br.com.apresentados.sprint02.javaoito.generics.wildcards.Fundacao;
import br.com.apresentados.sprint02.javaoito.generics.wildcards.Pessoa;

public class Classe <T>{

    // generics
    private T atributo;

    public T metodo(T param){
        return param;
    }

    // wildcards pode ser utilizado em atributos, metodos, etc.
    private SelecionadorAleatorio<? extends Pessoa> sapd;

    public SelecionadorAleatorio<?> metodo1(){
        return new SelecionadorAleatorio<String>();
    }

    public void metodo2(){
        SelecionadorAleatorio< ? super Fundacao> safa;
    }

}
