package br.com.apresentados.sprint02.javaoito.generics;

import java.util.ArrayList;
import java.util.List;

public class Generics {
    public static void main(String[] args) {
        // Java 1.4
        List lista = new ArrayList();
        lista.add("Palavra");
        lista.add(11L);

        for (Object obj : lista) {
            if (obj instanceof String) {
                System.out.println(obj);
            }
            if (obj instanceof Long) {
                System.out.println(obj);
            }
        }
        List<String> generics = new ArrayList<>(); // generics é algo que funciona somente em tempo de compilação para garantir que está usando o mesmo tipo
        generics.add("Aaa");
        generics.add("Bbb");
//        generics.add(1);
        for (String lista2 : generics) {
            System.out.println(lista2);
        }
        add(generics, 1L);

//        for (String lista2 : generics){
//            System.out.println(lista2);
//        }
    }

    public static void add(List lista, Long l) {
        lista.add(l);
    }
}

