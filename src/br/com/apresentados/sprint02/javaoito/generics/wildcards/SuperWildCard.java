package br.com.apresentados.sprint02.javaoito.generics.wildcards;

import br.com.apresentados.sprint02.javaoito.generics.SelecionadorAleatorio;

public class SuperWildCard {


    public Object sortear(SelecionadorAleatorio<? super PessoaJuridica> sa ){
        return sa.sortear();
    }
}
