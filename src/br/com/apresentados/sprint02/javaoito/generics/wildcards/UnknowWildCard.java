package br.com.apresentados.sprint02.javaoito.generics.wildcards;

import br.com.apresentados.sprint02.javaoito.generics.SelecionadorAleatorio;

public class UnknowWildCard {
    public Object sortear(SelecionadorAleatorio<?> sa ){ // Aceita qualquer um como parâmetro ? = Pessoa/Fundacao/etc.
        return sa.sortear();
    }
}
