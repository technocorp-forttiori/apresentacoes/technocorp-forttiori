package br.com.apresentados.sprint02.javaoito.generics.wildcards;

public class Fundacao extends PessoaJuridica {

    private double patrimonio;
    private String estatuto;

    public Fundacao(String nome, int idade, String cnpj, double patrimonio, String estatuto) {
        super(nome, idade, cnpj);
        this.patrimonio = patrimonio;
        this.estatuto = estatuto;
    }

    public double getPatrimonio() {
        return patrimonio;
    }

    public String getEstatuto() {
        return estatuto;
    }
}
