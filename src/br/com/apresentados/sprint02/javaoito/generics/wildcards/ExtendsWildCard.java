package br.com.apresentados.sprint02.javaoito.generics.wildcards;

import br.com.apresentados.sprint02.javaoito.generics.SelecionadorAleatorio;

public class ExtendsWildCard {

    public PessoaJuridica sortear(SelecionadorAleatorio<? extends PessoaJuridica> saj ){
        return saj.sortear();
    }
}
