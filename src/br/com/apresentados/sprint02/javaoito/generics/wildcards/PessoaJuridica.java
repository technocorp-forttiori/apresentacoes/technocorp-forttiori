package br.com.apresentados.sprint02.javaoito.generics.wildcards;

public class PessoaJuridica extends Pessoa {
    private String cnpj;
    public PessoaJuridica(String nome, int idade, String cnpj) {
        super(nome, idade);
        this.cnpj = cnpj;
    }

    public String getCnpj() {
        return cnpj;
    }
}
