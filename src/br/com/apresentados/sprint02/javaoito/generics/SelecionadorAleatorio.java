package br.com.apresentados.sprint02.javaoito.generics;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

public class SelecionadorAleatorio<T> {

    private List<T> elementos;
    private Random random;

    public SelecionadorAleatorio() {
        this.elementos = new ArrayList<T>();
        this.random = new Random();
    }

    public void addElem(T elem){
        this.elementos.add(elem);
    }
    public void delElem(T elem){
        this.elementos.remove(elem);
    }

    public T getElem(int pos) {
        return this.elementos.get( pos);
    }

    public T sortear(){
        int pos = this.random.nextInt(this.elementos.size());
        return this.elementos.get(pos);
    }
}
