package br.com.apresentados.sprint02.javaoito.generics;

public interface Interface<T> {

    T metodo(T param);
}
