package br.com.apresentados.sprint02.javaoito.methodreference;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class Java8MethodReference {

    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(1, 2, 3, 4, 5);
        list.stream()
                .forEach(System.out::println); // Method Reference - da mesma instancia, "OUT" dentro da classe System.
        list.stream()
                .forEach(System.out::println); // Sem Method Reference


        // Métodos Estáticos
        list.stream()
                // No método Tradicional recebe o "n" como argumento e passa ele como argumento
                // O Java consegue entender que quando estou referenciando um método eu quero que ele passe como parâmetro
                // o que recebi na função lambda

                // .map((n) -> multipliquePorDois(n)) // sem method reference
                .map(Java8MethodReference::multipliquePorDois) // com method reference - usando com o nome de uma classe.
                .forEach(System.out::println);


        // construtores
        list.stream()
                // Recebe o n como argumento e passe ele para um construtor.
                // .map((n)-> new BigDecimal(n)) // sem method
                .map(BigDecimal::new) // com method - com o construtor da classe BigDecimal
                .forEach(System.out::println);


        // várias instâncias.
        list.stream()
                // Recebe o "n" como argumento, e chama o método do "n".
                // O java consegue entender que quando passamos o Integer::doubleValue
                // estou querendo que ele chame o doublevalue da instância que está chegando no stream
                //.map((n) -> n.doubleValue()) // sem method
                .map(Integer::doubleValue) // com method - usando o Double Value da classe Integer.
                .forEach(System.out::println);


        // única instância
        // recebendo o method como argumaneto e passando como parâmetro de uma instância de uma mesma instancia.
        BigDecimal dois = new BigDecimal(2);
        list.stream()
                .map(BigDecimal::new)
                //.map((b) -> dois.multiply(b)) // sem method
                .map(dois::multiply) // com method - Chamando o Multiply sempre no mesmo objeto, da instÂncia 2.
                .forEach(System.out::println);
    }

    private static Integer multipliquePorDois(Integer n) {
        return n * 2;
    }
}
