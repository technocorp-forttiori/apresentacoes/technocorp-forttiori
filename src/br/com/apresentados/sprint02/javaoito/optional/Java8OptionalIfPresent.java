package br.com.apresentados.sprint02.javaoito.optional;

import java.util.Optional;

public class Java8OptionalIfPresent {
    public static void main(String[] args) {
        String s = "Teste";
//        String s = "1";
        Optional<Integer> numero = converteEmNumero(s);
        numero.ifPresent(n -> System.out.println(n)); // A expressão só é executada SE tiver um valor dentro.
    }
    public static Optional<Integer> converteEmNumero(String numeroStr){
        try{
            Integer integer = Integer.valueOf(numeroStr);
            return Optional.of(integer);
        } catch (Exception e){
            return Optional.empty();
        }
    }
}
