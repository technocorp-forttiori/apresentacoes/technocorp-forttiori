package br.com.apresentados.sprint02.javaoito.optional;

import java.util.Optional;

public class Java8OptionalofNullable {
    public static void main(String[] args) {
        String s = "Teste";
        Optional<Integer> numero = converteEmNumero(s); // JAVA 8
        System.out.println(numero);
    }
    public static Optional<Integer> converteEmNumero(String numeroStr){
        return Optional.ofNullable(null);
    }
}
