package br.com.apresentados.sprint02.javaoito.optional;

import java.util.OptionalInt;

public class Java8OptionalInt {
    public static void main(String[] args) {
        // Tem também o Optional Double, Long.
        String s = "Teste";
        //String s = "1";
        int numero = converteEmNumero(s)
                .orElseThrow(() -> new NullPointerException("Valor Vazio")); // verifica se contem o elemento, se não retorna o que existe no orElseGet.
        System.out.println(numero);
    }

    public static OptionalInt converteEmNumero(String numero) {
        try {
            Integer integer = Integer.parseInt(numero);
            return OptionalInt.of(integer);
        } catch (Exception e) {
            return OptionalInt.empty();
        }
    }
}
