package br.com.apresentados.sprint02.javaoito.optional;

import java.util.Optional;

public class Java8Optional {
    public static void main(String[] args) {
//        String s = "Teste";
        String s = "1";
        Optional<Integer> numero = converteEmNumero(s); // JAVA 8 -- Pode ter um valor ou não.(optional)
        System.out.println(numero); // Retorna numero Optional.
        System.out.println(numero.isPresent()); // retorna true ou false - Pois Optional pode ou não conter o valor.
        System.out.println(numero.get()); // Se quiser pegar o valor que está dentro do optional.

        //numero.ifPresent(n -> System.out.println(n)); // Só executa se tiver um valor dentro.
        converteEmNumero(s).ifPresent(n -> System.out.println(n)); // Operação com melhor código, mais limpo.
    }
    public static Optional<Integer> converteEmNumero(String numeroStr){
        try{
            Integer integer = Integer.valueOf(numeroStr);
            return Optional.of(integer);
        } catch (Exception e){
            return Optional.empty();
        }
    }
}
