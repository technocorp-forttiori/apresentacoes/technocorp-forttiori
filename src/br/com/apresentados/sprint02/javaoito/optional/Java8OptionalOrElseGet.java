package br.com.apresentados.sprint02.javaoito.optional;

import java.util.Optional;

public class Java8OptionalOrElseGet {
    public static void main(String[] args) {
        // A diferença do OrElse para OrElseGet é que o Get recebe uma função Lambda.
        String s = "Teste";
        // String s = "1";
        Integer numero = converteEmNumero(s)
        .orElseGet(() -> 5); // verifica se contem o elemento, se não retorna o que existe no orElseGet.
        System.out.println(numero);
    }

    public static Optional<Integer> converteEmNumero(String numero) {
        try {
            Integer integer = Integer.valueOf(numero);
            return Optional.of(integer);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
