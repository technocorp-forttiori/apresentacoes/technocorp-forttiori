package br.com.apresentados.sprint02.javaoito.optional;

import java.util.Optional;

public class Java8OrElseThrow {
    public static void main(String[] args) {
        String s = "Teste";
        //String s = "1";
        Integer numero = converteEmNumero(s)
                .orElseThrow(() -> new NullPointerException("Valor Vazio")); // verifica se contem o elemento, se não retorna o que existe no orElseGet.
        System.out.println(numero);
    }

    public static Optional<Integer> converteEmNumero(String numero) {
        try {
            Integer integer = Integer.valueOf(numero);
            return Optional.of(integer);
        } catch (Exception e) {
            return Optional.empty();
        }
    }
}
