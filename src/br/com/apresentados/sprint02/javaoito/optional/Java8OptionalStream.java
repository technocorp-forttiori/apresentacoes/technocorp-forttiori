package br.com.apresentados.sprint02.javaoito.optional;

import java.util.stream.Stream;

public class Java8OptionalStream {
    public static void main(String[] args) {
        Stream.of(1,2,3)
                .findFirst() // Retorna um Optional de Inteiro.
                .ifPresent(System.out::println);
    }
}
