package br.com.apresentados.sprint02.javaoito.optional;

public class Java7SemOptional {
    public static void main(String[] args) {
        String s = "15";
        Integer numero = converteEmNumero(s);
        System.out.println(numero);
    }

    private static Integer converteEmNumero(String numeroStr) {
        return Integer.valueOf(numeroStr);
    }
}
