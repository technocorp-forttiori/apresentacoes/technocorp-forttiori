package br.com.apresentados.sprint02.javaoito.lambda;

import java.util.stream.IntStream;

public class Java8LambdazForms {
    public static void main(String[] args) {

        //Parênteses
        Runnable runnable = () -> System.out.println("Teste expressão");

        IntStream.range(0,5)
                .filter((int n) -> n % 2 == 0)
                .reduce((n1 ,n2) -> n1 + n2)
                .ifPresent(System.out::println);

        //Chaves
        IntStream.range(0,5)
                .filter((int n) -> {
                    System.out.println("Teste expressão com chaves!");
                    return n % 2 == 0;
                })
                .forEach(System.out::println);
    }
}
