package br.com.apresentados.sprint02.javaoito.lambda;

import java.util.Arrays;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Java8LambdaStream {
    public static void main(String[] args) {
        // JAVA 8: Funções Lambda
        // API DE STREAM
        // Fluxo de Dados

        List<Integer> asList = Arrays.asList(1, 2, 3, 4);
        asList.stream()
                .filter(e -> e % 2 == 0)
                .forEach(e -> System.out.println("Expressão com lambda stream: " + e));

        asList.stream()
                .filter(new Predicate<Integer>() {
                    @Override
                    public boolean test(Integer e) {
                        return e % 2 == 0;
                    }
                }).forEach(new Consumer<Integer>() {
            @Override
            public void accept(Integer e) {
                System.out.println("Expressão sem lambda stream: " + e);
            }
        });
    }
}
