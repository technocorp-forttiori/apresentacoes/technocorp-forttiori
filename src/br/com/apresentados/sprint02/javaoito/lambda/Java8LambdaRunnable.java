package br.com.apresentados.sprint02.javaoito.lambda;

public class Java8LambdaRunnable {
    public static void main(String[] args) {

        // Exemplo Função sem Lambda Java 7
        new Thread(new Runnable() {
            @Override
            public void run() {
                System.out.println("Olá Mundo, sem Lambda");
            }
        }).start();

        // Java 8 : Funções Lambda

        new Thread(() -> System.out.println("Olá Mundo Lambda")).start();
    }
}
