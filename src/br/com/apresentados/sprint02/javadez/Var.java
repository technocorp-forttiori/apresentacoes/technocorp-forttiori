package br.com.apresentados.sprint02.javadez;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;

public class Var {
    public static void main(String[] args) {
        var var = "Teste!";
        var nomes = List.of("1");
        var a = new ArrayList<Integer>();
        var numero = 1;

        for (var i = 0; i < 5; i++) {
            numero += 1;
            System.out.println(numero);
        }
        try (var r1 = new Scanner("Teste");) {
            System.out.println("teste");
        } catch (Exception e) {
            System.out.println("Teste");
        }
    }
}
