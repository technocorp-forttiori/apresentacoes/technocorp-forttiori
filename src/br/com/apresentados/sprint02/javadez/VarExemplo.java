package br.com.apresentados.sprint02.javadez;

import java.util.List;
import java.util.Scanner;

public class VarExemplo {
    public static void main(String[] args) {
        pode();
        naoPode();
    }

    private static void pode() {
        //int i = 52; // Java 9
        var i = 52;
        var str = "Teste de String com Var";
        var s = new Scanner(str);
        var teste = getNome(); // não é muito utilizado, não deixa claro o tipo da variável.

        var list = List.of("Teste","TEste2");

        //Foreach
        for(var elemento : list){ // não deixa extremamente claro qual o tipo da variável -- Cuidado, sempre ver o TIPO da Lista
        }
        for(var j = 0; j < 50; j++){

        }
    }

    private static void naoPode() {
        // var string; //<- é obrigado a inicializar - pois o java não sabe o tipo da variável.
        // var teste = null; // é a mesma coisa que não declarar nada
        // Runnable r = () -> System.out.println("Teste Lambda"); // não pode inicializar com var
        // var r = () -> System.out.println("Teste Lambda");
        // var não pode ser utilizado como argumento de método ex : private static void naoPode(var nome){}
        // construtores não podem ser inicializados com var- o java não sabe o TIPO. ex: public VarExemplo(var nome){}
        // não pode ser utilizado como retorno de método
        // não pode ser inicializado como atributos ex: var nome = "Teste"; mas sim String nome = "Teste";
        // não pode ser usado com trycatch ex: try {} catch (var e){} -- O java precisa saber o tipo da exception.
    }
    private static String getNome(){
        return "Teste";
    }
}
