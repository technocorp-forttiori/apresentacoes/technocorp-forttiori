package br.com.apresentados.sprint02.javadez;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UnModDemo {
    public static void main(String[] args) {
        var list1 = Arrays.asList(1,2,3,4);
        var set1 = Set.of(1,2,3,4);
        var map1 = Map.of("a",1,"b",2,"c",3);
        var s1 = Stream.of(10,20,30,25);
        var s2 = Stream.of("Rover","Joyful","Depth","Hunter");

        var list2 = List.copyOf(list1);  // Recebe uma lista imutável como Cópia.
        //list2.add(5); // não é possível adicionar pois a cópia também é imutável.
        var map2 = Map.copyOf(map1);
        var set2 = Set.copyOf(set1);

        var listc = s1.collect(Collectors.toUnmodifiableList());
        //listc.add(123);

        var listd = s2.collect(Collectors.toUnmodifiableSet());

        var var = 50;


    }
}
