package br.com.apresentados.sprint02.javadez;

public class Container {
    public static void main(String[] args) {
        // Melhorias na execução de Containers.
        /*
        -XX:+UseContainerSupport -> Permite que a JVM Saiba que está rodando dentro de um container.
        -XX:ActiveProcessorCount=count -> Permite definir a quantidade de processadores
        -XX:InitialRAMPercentage
        -XX:MaxRAMPercentage
        -XX:MinRAMPercentage
         */
    }
}
