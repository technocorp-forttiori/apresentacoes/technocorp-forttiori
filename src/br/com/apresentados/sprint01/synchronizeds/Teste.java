package br.com.apresentados.sprint01.synchronizeds;

public class Teste {
    public static void main(String[] args) {
        // Utilizando o synchronized a thred 2 só executará após a finalização da thred 1.
        int[] array = {1,2,3,4,5};
        MinhaThreadSoma t1 = new MinhaThreadSoma("#1", array);
        MinhaThreadSoma t2 = new MinhaThreadSoma("#2", array);
    }
}
