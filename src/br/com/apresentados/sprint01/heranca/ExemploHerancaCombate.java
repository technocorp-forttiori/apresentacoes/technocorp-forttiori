package br.com.apresentados.sprint01.heranca;

public class ExemploHerancaCombate extends ExemploHeranca{
    private final int MAX_ARMAMENTO = 100;
    private final int MIN_ARMAMENTO = 0;
    private int qtdeArmamento;

    public ExemploHerancaCombate(String nome, int blindagem){
        super(nome);
        super.setArmamento(true);
        super.setBlindagem(blindagem);
        this.qtdeArmamento = 100;
    }
    private void setQtdeArmamento(int qtdeArmamento){
        this.qtdeArmamento += qtdeArmamento;
        if (this.qtdeArmamento > MAX_ARMAMENTO){
            this.qtdeArmamento = MAX_ARMAMENTO;
        }
        if (this.qtdeArmamento < MIN_ARMAMENTO){
            this.qtdeArmamento = MIN_ARMAMENTO;
        }

    }
    public int getQtdeArmamento(){
        return this.qtdeArmamento;
    }
    public void atirar(){
        if (this.qtdeArmamento > MIN_ARMAMENTO){
            setQtdeArmamento(-1);
        }else{
            System.out.println("Sem munição");
        }
    }
    public void info(){
        super.info();
        System.out.println("Qtde.Arm..........:" + this.qtdeArmamento);
    }
}
