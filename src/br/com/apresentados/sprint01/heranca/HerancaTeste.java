package br.com.apresentados.sprint01.heranca;

public class HerancaTeste {
    public static void main(String[] args) {
        ExemploHeranca corsa = new ExemploHeranca("Corsa");
        ExemploHeranca fiat = new ExemploHeranca("Fiat");

        ExemploHerancaCombate tanque = new ExemploHerancaCombate("Tanque", 100);
        ExemploHerancaCombate tanque2 = new ExemploHerancaCombate("Tanque 2", 50);

        corsa.setLigado(true);
        tanque.setLigado(true);
        tanque2.setLigado(true);

        tanque.atirar();
        tanque.atirar();
        tanque.atirar();
        tanque2.sofrerDano(30);
        tanque2.sofrerDano(30);
        corsa.sofrerDano(5);

        corsa.info();
        fiat.info();
        tanque.info();
        tanque2.info();
    }
}
