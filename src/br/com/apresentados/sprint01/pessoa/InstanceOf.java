package br.com.apresentados.sprint01.pessoa;

public class InstanceOf {
    public static void main(String[] args) {
        Pessoa pessoa = new Pessoa();
        Pessoa aluno = new Aluno();
        Pessoa professor = new Professor();

        if (pessoa instanceof Pessoa){
            System.out.println("Objeto do tipo Pessoa.");
        } else {
            System.out.println("Não é do tipo Pessoa.");
        }
        if (aluno instanceof Aluno){
            System.out.println("Objeto do tipo Aluno.");
        }
        if (professor instanceof Professor){
            System.out.println("Objeto do tipo Professor");
        }
    }
}
