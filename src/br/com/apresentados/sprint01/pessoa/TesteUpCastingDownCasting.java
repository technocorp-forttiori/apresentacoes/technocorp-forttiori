package br.com.apresentados.sprint01.pessoa;

public class TesteUpCastingDownCasting {
    public static void main(String[] args) {

        // UpCasting -- Ocorre quando um objeto se passa por um objeto que seja um supertipo dele.
        Aluno aluno = new Aluno();
        Pessoa pessoaAluno = new Aluno(); // UpCasting
        // Ou
        Pessoa aluno2 = (Pessoa) new Aluno(); // UpCasting, geralmente usado em banco de Dados.

        //Super classe Object é a superclasse de todas as classes do Java
        Object obj1 = obterString();
        String s1 = (String) obj1;

        Object obj2 = "Minha String"; // Upcasting

        // DownCasting - Ocorre quando o objeto se passa como se fosse um subtipo dele.
        //Object obj3 = new Object();  // Dá erro em tempo de execução, pois
        //String s3 = (String) obj3;   // Object não faz referência a uma String

        //Object obj4 = obterInteiro(); // Erro de execução pois estou obtendo um inteiro
        //String s4 = (String) obj4;    // não tem nada a ver como uma string. Porém há como
    }                                 // Transformar o inteiro em String, desta forma funcionaria.

    public static String obterString() {
        return "minha String";
    }

    public static int obterInteiro() {
        return 1;
    }
}
