package br.com.apresentados.sprint01.classeabstrata;

import br.com.apresentados.sprint01.interfaces.ExemploInterfaceSerVivo;

public class Formiga extends AnimalAbstrata implements ExemploInterfaceSerVivo {
    public Formiga(int massa, int vel, int forca) {
        super(massa, vel, forca);
    }
}
