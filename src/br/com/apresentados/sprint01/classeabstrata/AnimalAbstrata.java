package br.com.apresentados.sprint01.classeabstrata;
//As classes abstratas são as que não permitem realizar qualquer tipo de instância.
// São classes feitas especialmente para serem modelos para suas classes derivadas.
// As classes derivadas, via de regra, deverão sobrescrever os métodos para realizar a
// implementação dos mesmos.
// Exemplo: AnimalAbstrata teste = new AnimalAbstrata();

import br.com.apresentados.sprint01.interfaces.ExemploInterfaceSerVivo;

public abstract class AnimalAbstrata implements ExemploInterfaceSerVivo {
    private Boolean vivo;
    private int massa;
    private int x;
    private int y;
    private int vel;
    private int forca;

    public AnimalAbstrata(int massa, int vel, int forca) {
        this.vivo = true;
        this.massa = massa;
        this.x = 0;
        this.y = 0;
        this.vel = vel;
        this.forca = forca;
    }

    public void mover() {
        if (this.vivo) {
            this.x += this.vel;
            this.y += this.vel;
        } else {
            System.out.println("---------------------------");
            System.out.println(this.getClass().toGenericString() + '\n' + "Está Morto, não pode mover!");
            System.out.println("---------------------------");
        }

    }

    public void atacar(AnimalAbstrata animalAbstrata) {
        if (this.vivo) {
            if (this.forca > animalAbstrata.forca) {
                this.forca += animalAbstrata.getMassa();
                animalAbstrata.vivo = false;
            } else {
                this.vivo = false;
            }
        } else {
            System.out.println("---------------------------");
            System.out.println(this.getClass().toGenericString() + '\n' + "Está Morto, não pode atacar!");
            System.out.println("---------------------------");
        }
    }

    public void comer(int massa) {
        if (this.vivo) {
            this.forca += massa;
        } else {
            System.out.println("---------------------------");
            System.out.println(this.getClass().toGenericString() + '\n' + "Está Morto, não pode comer!");
            System.out.println("---------------------------");
        }
    }

    public void info() {
        System.out.printf("Tipo..:%s%n",getClass().toString());
        System.out.printf("Vivo..:%s%n",this.getVivo() ? "Sim" : "Não");
        System.out.printf("Massa.:%d%n",this.massa);
        System.out.printf("Vel...:%s%n",this.vel);
        System.out.printf("Força.:%s%n",this.forca);
        System.out.println("---------------------------");
    }

    public void setVivo(Boolean vivo) {
        this.vivo = vivo;
    }

    public Boolean getVivo() {
        return vivo;
    }

    public void setForca(int forca) {
        this.forca = forca;
    }

    public int getForca() {
        return forca;
    }

    public int getMassa() {
        return massa;
    }

    public void setMassa(int massa) {
        this.massa = massa;
    }
}
