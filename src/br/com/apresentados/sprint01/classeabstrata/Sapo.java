package br.com.apresentados.sprint01.classeabstrata;

import br.com.apresentados.sprint01.interfaces.ExemploInterfaceSerVivo;

public class Sapo extends AnimalAbstrata implements ExemploInterfaceSerVivo{
    public Sapo(int massa, int vel, int forca) {
        super(massa, vel, forca);
    }

}
