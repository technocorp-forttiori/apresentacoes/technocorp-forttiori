package br.com.apresentados.sprint01.classeabstrata;

public class Aranha extends AnimalAbstrata {
    private int veneno;

    public Aranha(int massa, int vel, int forca, int veneno) {
        super(massa, vel, forca);
        this.veneno = veneno;
    }
    @Override
    public void atacar(AnimalAbstrata animalAbstrata){
        if (this.getVivo()) {
            if(this.getForca() <= animalAbstrata.getForca()){
                this.setForca(this.getForca() + animalAbstrata.getMassa());
            }
            if ((this.getForca() + this.veneno) > animalAbstrata.getForca()) {
          //      this.setForca(this.getForca() + animalAbstrata.getMassa());
                animalAbstrata.setVivo(false);
            } else {
                this.setVivo(false);
            }
        } else {
            System.out.println("---------------------------");
            System.out.println(this.getClass().toGenericString() + "Está Morto, não pode atacar!");
            System.out.println("---------------------------");
        }
    }
    @Override
    public void info(){
        super.info();

    }
}
