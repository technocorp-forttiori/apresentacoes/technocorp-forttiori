package br.com.apresentados.sprint01.classeabstrata;

public class ClasseAbstrataTeste {
    public static void main(String[] args) {
        //Não é possível instanciar pois é uma classe abstrata.
        //AnimalAbstrata animalAbstrata = new AnimalAbstrata();

        Formiga formiga = new Formiga(5, 3, 2);
        Vegetal vegetal = new Vegetal(1);
        Vegetal vegetal2 = new Vegetal(2);
        Aranha aranha = new Aranha(10,10,10,3);

        aranha.info();
        aranha.atacar(formiga);
        formiga.mover();
        aranha.comer(formiga.getMassa());
        aranha.info();
    }
}
