package br.com.apresentados.sprint01.statics;

public class JogadorTeste {
    public static void main(String[] args) {
        // Buscam e Salvam no mesmo Endereço de Memória

        int num = 0;

        // System.out.println("Alerta:" + JogadorExemploStatic.alerta);

        // Método Static Global;
        JogadorExemploStatic.pontos();
        JogadorExemploStatic.pontos();

        JogadorExemploStatic j1 = new JogadorExemploStatic(++num);
        JogadorExemploStatic j2 = new JogadorExemploStatic(++num);
        JogadorExemploStatic j3 = new JogadorExemploStatic(++num);

        JogadorExemploStatic.alerta = true;

        j1.info();
        j2.info();
        j3.info();
    }
    /*
     * Todos os elementos statics, atributos ou métodos tem o mesmo endereço de memória.
     * Vai ser comum para todos objetos da classe.
     * Não é necessário instanciar uma classe para chamar o método ou atributo static
     * */
}
