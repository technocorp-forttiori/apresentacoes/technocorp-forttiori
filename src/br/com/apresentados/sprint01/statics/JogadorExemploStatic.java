package br.com.apresentados.sprint01.statics;

public class JogadorExemploStatic {
    //Os métodos static ou métodos da classe são funções
    // que não dependem de nenhuma variável de instância, quando
    // invocados executam uma função sem a dependência do conteúdo de um
    // objeto ou a execução da instância de uma classe, conseguindo
    // chamar direto qualquer método da classe e também manipulando
    // alguns campos da classe.

    private final int maxVidas = 3;
    private int num = 0;
    private int vidas = 0;
    public static boolean alerta = false;
    static int qtdJogadores = 0;
    static int pontosJogadores = 0;

    public JogadorExemploStatic(int num) {
        this.num = num;
        this.vidas = 1;
        qtdJogadores ++;
        System.out.println("Jogador numero criado n" + num + '\n');
    }
    public int getVidas(){
        return this.vidas;
    }
    public void addVidas(){
        if(this.vidas < maxVidas){
            this.vidas ++;
        }
    }

    // método static para todos jogadores
    public static void pontos(){
        pontosJogadores+= 10;
    }

    public void info(){
        System.out.println("Vidas: " + this.vidas);
        System.out.println("Alerta: " + (alerta ? "Sim" : "Não"));
        System.out.println("Jogadores: " + qtdJogadores);
        System.out.println("Pontos Jogadores: " + pontosJogadores);
        System.out.println('\n');
    }
}
