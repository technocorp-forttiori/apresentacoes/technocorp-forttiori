package br.com.apresentados.sprint01.finaltest;

// final variable, method , class
public class Final {
    public static void main(String[] args) {
        final int i = 5;
        // i = 10; // final não pode ser mudado após ser criado, pois é uma constante
        System.out.println(i);
    }
}
