package br.com.apresentados.sprint01.polimorfismo;

public class EmpregadoMensalista extends EmpregadoPolimorfismo{
    private int salarioMensal;
    private double taxaEncargosTrabalhistas;

    EmpregadoMensalista(int salario, double d){
        salarioMensal = salario;
        taxaEncargosTrabalhistas = d;
    }
    @Override
    int pagamentoDoMes() {
        return (int)(salarioMensal * taxaEncargosTrabalhistas);
    }
    public String toString(){
        return "Sou empregado Mensalista, salarário Mensal R$" + salarioMensal;
    }
}
