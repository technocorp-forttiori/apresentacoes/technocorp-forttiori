package br.com.apresentados.sprint01.polimorfismo;

import java.util.ArrayList;
import java.util.List;

abstract public class EmpregadoPolimorfismo {

    abstract int pagamentoDoMes();
    public static void main(String[] args) {
        List<EmpregadoPolimorfismo> corpoDeTrabalho = new ArrayList<>();

        corpoDeTrabalho.add(new EmpregadoHorista(100, 30));
        corpoDeTrabalho.add(new EmpregadoMensalista(5000, 1.8));
        corpoDeTrabalho.add(new EmpregadoAvulso(7000));

        int custoTotal = 0;
        for (EmpregadoPolimorfismo trabalhador : corpoDeTrabalho)
            custoTotal += trabalhador.pagamentoDoMes(); // Polimorfismo
        System.out.println("Minha folha de pagamento nesse mês vai custar: " + custoTotal);
        System.out.println(corpoDeTrabalho);
    }
}
