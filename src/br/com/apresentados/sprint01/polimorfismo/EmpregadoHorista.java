package br.com.apresentados.sprint01.polimorfismo;

public class EmpregadoHorista extends EmpregadoPolimorfismo{
    private int salarioPorHora;
    private int horasTrabalhadas;

    EmpregadoHorista(int salarioPorHora, int horas){
        this.salarioPorHora = salarioPorHora;
        this.horasTrabalhadas = horas;
    }
    @Override
    int pagamentoDoMes(){
        return salarioPorHora + horasTrabalhadas;
    }
    public String toString(){
        return "Sou empregado horista, por hora R$" + salarioPorHora + " trabalhei " + horasTrabalhadas + " Horas";
    }
}
