package br.com.apresentados.sprint01.polimorfismo;

public class EmpregadoAvulso extends EmpregadoPolimorfismo{
    private int pagamento;
    public EmpregadoAvulso(int valor) {
        pagamento = valor;
    }
    @Override
    int pagamentoDoMes(){
        return pagamento;
    }
    public String toString(){
        return "Sou empregado avulso e cobrei R$" + pagamento;
    }
}
