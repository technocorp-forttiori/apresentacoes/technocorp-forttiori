package br.com.apresentados.sprint01.interfaces;

import br.com.apresentados.sprint01.classeabstrata.AnimalAbstrata;

public interface ExemploInterfaceSerVivo {
    public boolean vivo = true;
    public void mover();
    public void comer(int massa);
    public void info();
    public void atacar(AnimalAbstrata animalAbstrata);
}
