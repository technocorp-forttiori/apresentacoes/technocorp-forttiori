package br.com.apresentados.sprint01.interfaces;

import br.com.apresentados.sprint01.classeabstrata.AnimalAbstrata;

public class Animal implements ExemploInterfaceSerVivo, TesteMaisInterface {
    public Animal(){

    }

    @Override
    public void mover() {

    }

    @Override
    public void comer(int massa) {

    }

    @Override
    public void info() {

    }

    @Override
    public void atacar(AnimalAbstrata animalAbstrata) {

    }
}
