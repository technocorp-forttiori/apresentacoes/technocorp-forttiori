package br.com.apresentados.sprint01.threadsrunnable;

public class MinhaThreadRunnableJoin {
    public static void main(String[] args) {


        MinhaThreadRunnable threadRunnable1 = new MinhaThreadRunnable("#1", 500);
        MinhaThreadRunnable threadRunnable2 = new MinhaThreadRunnable("#2", 500);
        MinhaThreadRunnable threadRunnable3 = new MinhaThreadRunnable("#3", 500);

        Thread t1 = new Thread(threadRunnable1);
        Thread t2 = new Thread(threadRunnable2);
        Thread t3 = new Thread(threadRunnable3);

        t1.start();
        try {
            t1.join(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        t2.start();
        try {
            t1.join(200);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        t3.start();

        try {
            t1.join();
            t2.join();
            t3.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println("Programa finalizado");
    }
}
