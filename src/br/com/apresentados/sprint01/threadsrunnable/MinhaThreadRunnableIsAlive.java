package br.com.apresentados.sprint01.threadsrunnable;

public class MinhaThreadRunnableIsAlive {
    public static void main(String[] args) {


        MinhaThreadRunnable threadRunnable1 = new MinhaThreadRunnable("#1", 500);
        MinhaThreadRunnable threadRunnable2 = new MinhaThreadRunnable("#2", 500);
        MinhaThreadRunnable threadRunnable3 = new MinhaThreadRunnable("#3", 500);

        Thread t1 = new Thread(threadRunnable1);
        Thread t2 = new Thread(threadRunnable2);
        Thread t3 = new Thread(threadRunnable3);

        t1.start();
        t2.start();
        t3.start();

        while(t1.isAlive() || t2.isAlive() || t3.isAlive()){ // Enquanto as Threads Estiverem vivas
                                                             // Programa Espera para imprimir as msg.
            try{
                Thread.sleep(200);
            } catch (InterruptedException e){
                e.printStackTrace();
            }
        }
        System.out.println("Programa finalizado");
    }
}
