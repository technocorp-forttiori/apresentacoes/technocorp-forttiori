package br.com.apresentados.sprint01.acessoparametroecopia;

class Carro {
    int ano;
    String placa;
}

public class MyTest {
    public static void main(String[] args) {
        // A passagem por valor tem a ideia de que, na memória, tem a variável valor.
        // Quando é instanciado, há um ponteiro apontando pra ela.
        // Quando é passado por parâmetro, é criado um novo ponteiro que aponta para
        // uma cópia dela, ou seja é uma nova variável que começa com o valor  50.
        int valor = 50;
        dobrarValor(valor);
        int valor2 = valor;
        System.out.println(valor);
        dobrarValor(valor2);
        System.out.println(valor2);

        // Quando é passado um objeto em java, é feita uma passagem por referência.
        // É apontado um ponteiro para o mesmo objeto, e acrescenta um valor para o objeto.
        Carro c = new Carro();
        c.ano = 1997;
        mudarAnoCarro(c);
        System.out.println(c.ano);
    }

    public static void dobrarValor(int valor) {
        valor = valor * 2;
    }

    public static void mudarAnoCarro(Carro c) {
        c.ano = c.ano + 1; }
}
