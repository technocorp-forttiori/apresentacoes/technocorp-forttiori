package br.com.apresentados.sprint01.acessoparametroecopia;

public class PassagemValorParametro {
    public static void main(String[] args) {
        ContatoAcesso contato = new ContatoAcesso("Contato 1", "1234-5678", "contato01@email.com");
        int valor = 10;

        System.out.println("Valores Originais");
        System.out.println(contato);
        System.out.println(valor);

        testePassagemValorReferencia(valor, contato);

        System.out.println("Exemplo 1");
        System.out.println(contato);
        System.out.println(valor);

        testePassagemValorReferencia2(valor, contato);

        System.out.println("Exemplo 2");
        System.out.println(contato);
        System.out.println(valor);


    }
    private static void testePassagemValorReferencia(int valor, ContatoAcesso contato){
        int novoValor = valor +10;
        valor = novoValor;

        contato = new ContatoAcesso("Contato 2","2345-5895","testecontato@email.com");

    }
    private static void testePassagemValorReferencia2(int valor, ContatoAcesso contato){
        int novoValor = valor + 10;
        valor = novoValor;

        contato.setNome("Contato"+novoValor);
    }

}
