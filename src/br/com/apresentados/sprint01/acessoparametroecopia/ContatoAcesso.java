package br.com.apresentados.sprint01.acessoparametroecopia;

public class ContatoAcesso {
    private String nome;
    private String telefone;
    private String email;

    public ContatoAcesso(String nome, String telefone, String email) {
        super();
        this.nome = nome;
        this.telefone = telefone;
        this.email = email;
    }

    public ContatoAcesso(){}

    public String getNome() {
        return nome;
    }

    public String getTelefone() {
        return telefone;
    }

    public String getEmail() {
        return email;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String toString(){
        return "Contato [nome = " + nome + ", telefone = " + telefone + ", email = " + email + "]";
    }
}
