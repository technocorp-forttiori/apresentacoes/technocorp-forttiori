package br.com.apresentados.sprint01.enums;

public class Curso {

    private String nome;
    private Turno turno;
    // Turno tem um nome conhecido, pode ser apenas Manhã, Tarde ou Noite.
    // Pois são as constantes definidas.
    private int horas;

    public String getNome(){
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Turno getTurno() {
        return turno;
    }

    public void setTurno(Turno turno) {
        this.turno = turno;
    }

    public int getHoras() {
        return horas;
    }

    public void setHoras(int horas) {
        this.horas = horas;
    }

    @Override
    public String toString() {
        return "{" +
                "nome='" + nome + '\'' +
                ", turno=" + turno +
                ", horas=" + horas +
                '}';
    }
}
