package br.com.apresentados.sprint01.enums;

public enum TipoEmpregado {
    // São tipos de campos que consistem em um conjunto fixo de
    // constantes (static final), sendo como uma lista de valores pré-definidos.
    MENSALISTA, HORISTA, AVULSO
}
