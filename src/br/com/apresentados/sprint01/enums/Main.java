package br.com.apresentados.sprint01.enums;

public class Main {
    public static void main(String[] args) {
        Curso curso = new Curso();
        curso.setNome("Análise e Desenvolvimento de Sistemas");
        curso.setHoras(3600);
        curso.setTurno(Turno.MANHA);

        System.out.println(curso);
    }
}
