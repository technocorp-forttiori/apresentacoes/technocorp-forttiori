package br.com.apresentados.sprint01.enums;

public enum Turno {
    // Enum é utilizado para definir o conjunto de valores que podem ser
    // Atribuídos a uma variável.
    MANHA("Manhã"),
    TARDE("Tarde"),
    NOITE("Noite");

    private String descricao;

    Turno(String descricao){
        this.descricao = descricao;
    }
    public String getDescricao(){return descricao;}
}
