package br.com.apresentados.sprint01.exception;

public class UsandoException {
    public static void main(String[] args) {
        int[] numeros = {4, 8, 5, 16, 32, 21, 64, 128};
        int[] denom = {2, 0, 4, 8, 0, 2, 3};

        for (int i = 0; i < numeros.length; i++) {
            try {
                if (numeros[i] % 2 != 0) {
                    //lancar exception
                    throw new Exception("Numero impar, Divisão não exata!");
                }
                System.out.println(numeros[i] + "/" + denom[i] + " = " + (numeros[i] / denom[i]));
            } catch (ArithmeticException | ArrayIndexOutOfBoundsException e) {
                System.out.println("Aconteceu um Erro!");
            } catch (Exception e) {
                System.out.println("Aconteceu um erro!");
                System.out.println(e.getMessage());
            }
        }

    }
}
