package br.com.apresentados.sprint01.constructor;


import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public class Carro {

    private String cor;
    private double preco;
    private String modelo;

    /* CONSTRUTOR PADRÃO */
    public Carro() {
    }

    /* CONSTRUTOR COM 2 PARÂMETROS */
    public Carro(String modelo, double preco) {
        //Se for escolhido o construtor sem a COR do veículo
        // definimos a cor padrão como sendo PRETA
        this.cor = "PRETA";
        this.modelo = modelo;
        this.preco = preco;

    }

    /* CONSTRUTOR COM 3 PARÂMETROS */
    public Carro(String cor, String modelo, double preco) {
        this.cor = cor;
        this.modelo = modelo;
        this.preco = preco;
    }

    public String getCor() {
        return cor;
    }

    public void setCor(String cor) {
        this.cor = cor;
    }

    public double getPreco() {
        return preco;
    }

    public void setPreco(double preco) {
        this.preco = preco;
    }

    public String getModelo() {
        return modelo;
    }

    public void setModelo(String modelo) {
        this.modelo = modelo;
    }
}