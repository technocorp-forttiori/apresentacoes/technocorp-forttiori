package br.com.apresentados.sprint01.constructor;

public class CarroTest {
     /*
    Encapsulamento: Default, Public, Protected e Private

    Public : Todos tem acesso.

    Default: Tem acesso a um atributo default (identificado pela ausência de modificadores)
    todas as classes que estiverem no mesmo pacote que a classe que possui o atributo.

    Protected : Protected: Esse é o que pega mais gente, ele é praticamente igual ao default, com a
    diferença de que se uma classe (mesmo que esteja fora do pacote)
    estende da classe com o atributo protected, ela terá acesso a ele. Então o acesso é por pacote e por herança.

    Private: A única classe que tem acesso ao atributo é a própria classe que o define, ou seja, se uma classe
    Pessoa declara um atributo privado chamado nome, somente a classe Pessoa terá acesso a ele.

    O nível de visibilidade envolve encapsulamento. É sempre dito como boa prática que atributos
    internos devem ser privados, pois classes externas nem devem saber que ele existe. O que a classe
    expõe são suas funcionalidades, sua API, se preferir. Expor atributos internos pode causar sérios
    problemas de segurança. Se tem algo que é inerente à implementação, que pode vir a mudar no futuro.
    provavelmente deve ser privado.
    */
    // Exemplos de Construtores, PADRÃO / COM PARÂMETROS
    public static void main(String[] args) {

        Carro honda = new Carro();
        honda.setModelo("Honda");
        honda.setCor("Azul");
        honda.setPreco(10000);

        System.out.println("Modelo: " + honda.getModelo() + '\n' + "Cor: " + honda.getCor() + '\n' + "Preço: " + honda.getPreco() + '\n');

        Carro fiat = new Carro("Fiat", 100.00);
        System.out.println("Modelo: " + fiat.getModelo() + '\n' + "Cor: " + fiat.getCor() + '\n' + "Preço: " + fiat.getPreco() + '\n');

        // Exemplo com 3 parâmetros.

        Carro alpha = new Carro("Preta","Fiat",10);
        System.out.println("Modelo: " + alpha.getModelo() + '\n' + "Cor: " + alpha.getCor() + '\n' + "Preço: " + alpha.getPreco() + '\n');
    }
}
