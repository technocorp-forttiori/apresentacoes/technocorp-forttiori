package br.com.apresentados.sprint01.estruturadedados.queue;

import java.util.LinkedList;
import java.util.Queue;

public class BBQ {
    public static void main(String[] args) {
        // First In, First Out - FIFO
        Queue<String> bbqFila = new LinkedList<>();

        bbqFila.add("Matheus");
        bbqFila.add("Lucas");
        bbqFila.add("Douglas");
        bbqFila.add("Diego");

        System.out.println("O tamanho atual da fila é: " + bbqFila.size()+ "\n");

        System.out.println(bbqFila);

        // para verificar a fila peek
        System.out.println("O primeiro da fila é: " + bbqFila.peek());

        // para remover o primeiro da lista .poll

        System.out.println("\nA pessoa que saiu da fila é: " + bbqFila.poll());

        System.out.println(bbqFila);

        // tamanho da fila

        System.out.println("O tamanho atual da fila é: " + bbqFila.size());

    }
}
