package br.com.apresentados.sprint01.estruturadedados.arraylist;

import java.util.ArrayList;
import java.util.List;

public class ArrayListClass {
    public static void main(String[] args) {
        List<String> arrayList = new ArrayList<>();

        arrayList.add("A");
        arrayList.add("C");
        arrayList.add("E");

        var teste = arrayList.contains("A") ? "Existe o elemento" : "Não existe o elemento";

        System.out.println(arrayList);
        System.out.println(arrayList.contains("A"));
        System.out.println(teste);

        int pos = arrayList.indexOf("A");
        if (pos > -1) {
            System.out.println("Elemento existe no array");
        } else {
            System.out.println("Elemento não existe no array");
        }

        arrayList.remove(1);
        arrayList.add(0,"A");
        System.out.println(arrayList);

        arrayList.clear();
        System.out.println(arrayList);
    }
}
