package br.com.apresentados.sprint01.estruturadedados.linkedlist;

import java.util.List;

public class LinkedList {
    public static void main(String[] args) {
        List<String> linkedList = new java.util.LinkedList<>();

        linkedList.add("A");
        linkedList.add("C");
        linkedList.add("E");

        var teste = linkedList.contains("A") ? "Existe o elemento" : "Não existe o elemento";

        System.out.println(linkedList);
        System.out.println(linkedList.contains("A"));
        System.out.println(teste);

        int pos = linkedList.indexOf("A");
        if (pos > -1) {
            System.out.println("Elemento existe no array");
        } else {
            System.out.println("Elemento não existe no array");
        }

        linkedList.remove(1);
        linkedList.add(0,"A");
        System.out.println(linkedList);

        linkedList.clear();
        System.out.println(linkedList);
    }
}
