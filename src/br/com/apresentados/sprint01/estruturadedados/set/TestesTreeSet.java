package br.com.apresentados.sprint01.estruturadedados.set;

import java.util.HashSet;
import java.util.Set;
import java.util.TreeSet;

public class TestesTreeSet {
    public static void main(String[] args) {

        long startTimeHashSet = System.nanoTime();
        Set<String> cursos = new HashSet<>();
        cursos.add("PHP");
        cursos.add("Java");
        cursos.add("PL/SQL");
        cursos.add("Adobe");
//        cursos.add("PHP");
        long endTimeHashSet = System.nanoTime();

        System.out.println("O tempo de adição dos curos com HashSet é de: " + (endTimeHashSet - startTimeHashSet));

        long startTime = System.nanoTime();
        Set<String> ord = new TreeSet<String>(cursos);
        long endTime = System.nanoTime();
        System.out.println("O tempo de execução do TreeSet é de: " + (endTime - startTime));
        System.out.println("Sem ordenação: "+cursos);
        System.out.println("Com ordenação: "+ord);
    }
}