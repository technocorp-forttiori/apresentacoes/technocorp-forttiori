package br.com.apresentados.sprint01.estruturadedados.set;

import java.util.Set;
import java.util.TreeSet;

public class TestesSet {
    public static void main(String[] args) {
        Set<String> colecaoSet = new TreeSet<>();
        colecaoSet.add("Site");
        colecaoSet.add("Varallos");
        colecaoSet.add("Fóruns Varallos");

        //dados duplicados não são inseridos no Set
        colecaoSet.add("Varallos");
        colecaoSet.add("Site");

        System.out.println("Tamanho coleção Set: "+colecaoSet.size());
        int count = 0;
        for(String valor : colecaoSet){
            System.out.println(++count + " -> " + valor);
        }
    }
}