package br.com.apresentados.sprint01.estruturadedados.set;

import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;

public class TesteLinkedHashSet {
    public static void main(String[] args) {
        Set<Cliente> lhs = new LinkedHashSet<>();
        final long startTime = System.nanoTime();
        lhs.add(new Cliente("João Delfino","Rua da Várzea","3232-1232"));
        lhs.add(new Cliente("Maria Tijuca","Av. Brasil","8569-99988"));
        lhs.add(new Cliente("Pedro de Lara","Rua 20 de março","7568-8524"));
        final long endTime = System.nanoTime();

        final long timeTotal = endTime - startTime;

        Cliente clienteJoao = new Cliente("João Delfino","Rua da Várzea","3232-1232");
        if(lhs.contains(clienteJoao)){
            System.out.println("Existe o cliente João Delfino");
        }
        System.out.println("O tempo para adicionar os clientes é de: " + timeTotal);

        System.out.println("Tamanho coleção HashSet: "+lhs.size());

        //Percorrendo o HashSet<Cliente> e imprimindo os valores
        Iterator<Cliente> it = lhs.iterator();
        while(it.hasNext()){
            Cliente valorCliente = (Cliente)it.next();
            System.out.println(valorCliente);
        }
    }
}
