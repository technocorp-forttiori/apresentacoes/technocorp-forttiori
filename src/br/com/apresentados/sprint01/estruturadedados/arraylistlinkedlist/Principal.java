package br.com.apresentados.sprint01.estruturadedados.arraylistlinkedlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class Principal {

    public static void main(String args[]) {

        ArrayList arrayList = new ArrayList();
        LinkedList linkedList = new LinkedList();

        System.out.println("ArrayList vs LinkedList\n");

        // ArrayList add
        long startTime = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            arrayList.add(i);
        }
        long endTime = System.nanoTime();
        long duration = endTime - startTime;
        System.out.println("ArrayList add:  " + duration);

        // LinkedList add
        startTime = System.nanoTime();

        for (int i = 0; i < 100000; i++) {
            linkedList.add(i);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList add: " + duration);

        // ArrayList get
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            arrayList.get(5000);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("ArrayList get:  " + duration);

        // LinkedList get
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            linkedList.get(5000);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList get: " + duration);

        // ArrayList set
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            Object set = arrayList.set((int) Math.random()," ");
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("ArrayList set:  " + duration);

        // LinkedList set
        startTime = System.nanoTime();

        for (int i = 0; i < 10000; i++) {
            Object set = linkedList.set((int) Math.random(), " ");
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList set: " + duration);

        // ArrayList remove
        startTime = System.nanoTime();

        for (int i = 9999; i >= 0; i--) {
            arrayList.remove(500);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("ArrayList remove:  " + duration);

        // LinkedList remove
        startTime = System.nanoTime();

        for (int i = 9999; i >= 0; i--) {
            linkedList.remove(500);
        }
        endTime = System.nanoTime();
        duration = endTime - startTime;
        System.out.println("LinkedList remove: " + duration);

    }

}