package br.com.apresentados.sprint01.estruturadedados.arraylistlinkedlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayListVsLinkedListSetRemove {
    public static void main(String[] args) {
        ArrayList<Integer> arrayListRemove = new ArrayList<>();
        LinkedList<Integer> linkedListRemove = new LinkedList<>();
        long startTimeAL;
        long endTimeAL;
        long startTimeLL;
        long endTimeLL;
        for (int i = 0; i < 1000000; i++){
            arrayListRemove.add(i);
            linkedListRemove.add(i);
        }

        startTimeAL = System.nanoTime();
        arrayListRemove.remove(500000);
        endTimeAL = System.nanoTime();

        startTimeLL = System.nanoTime();
        linkedListRemove.remove(500000);
        endTimeLL = System.nanoTime();

        System.out.println("\nO tempo total de remoção do ArrayList é: " + ( endTimeAL - startTimeAL));

        System.out.println("\nO Tempo do LinkedList é maior porque a linkedlist não pode ir direto para o indice que" +
                " vai excluir. Precisa ir até o caminho e depois excluir");
        System.out.println("O tempo total de remoção do LinkedList é: " + (endTimeLL - startTimeLL));

        // SET ARRAYLIST
        startTimeAL = System.nanoTime();
        arrayListRemove.set(50000,50000);
        endTimeAL = System.nanoTime();
        System.out.println("O tempo para um set no arraylist no meio é de: " + (endTimeAL - startTimeAL));
        // SET LINKEDLIST
        startTimeLL = System.nanoTime();
        arrayListRemove.set(50000,50000);
        endTimeLL = System.nanoTime();
        System.out.println("O tempo para um set no linkedlist no meio é de: " + (endTimeLL - startTimeLL));

    }
}
