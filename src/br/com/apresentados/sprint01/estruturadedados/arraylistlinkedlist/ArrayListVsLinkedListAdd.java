package br.com.apresentados.sprint01.estruturadedados.arraylistlinkedlist;

import java.util.ArrayList;
import java.util.LinkedList;

public class ArrayListVsLinkedListAdd {
    public static void main(String[] args) {
        ArrayList<Integer> arrayList = new ArrayList<>();
        LinkedList<Integer> linkedList = new LinkedList<>();

        for (int i = 0; i < 10000; i++){
            arrayList.add(i);
            linkedList.add(i);
        }

        final long startTimeAL = System.nanoTime();
        arrayList.add(100);
        final long endTimeAL = System.nanoTime();

        final long startTimeLL = System.nanoTime();
        linkedList.add(100);
        final long endTimeLL = System.nanoTime();

        long totalTimeAL = endTimeAL - startTimeAL;
        long totalTimeLL = endTimeLL - startTimeLL;

        System.out.println("O tempo total de execução de ArrayList é: " + totalTimeAL);
        System.out.println("O tempo total de execução de LinkedList é: " + totalTimeLL);
    }

}
