package br.com.apresentados.sprint01.estruturadedados.arrayobject;

public class ArrayObjectTest {
    public static void main(String[] args) {
        ArrayObject array = new ArrayObject(3);
        array.adiciona2(3);
        array.adiciona2(4);
        array.adiciona2(5);

        System.out.println("Tamanho = " + array.tamanho());
        System.out.println(array);

        ArrayObject array2 = new ArrayObject(5);

        Contato c1 = new Contato("Contato 1","1234-0000","contato1@email.com");
        Contato c2 = new Contato("Contato 2","1234-1111","contato2@email.com");
        Contato c3 = new Contato("Contato 3","1234-2222","contato3@email.com");

        Contato c4 = new Contato("Contato 4","2134-2134","contato4@email.com");

        array2.busca2(c4);

        array2.adiciona2(c1);
        array2.adiciona2(c2);
        array2.adiciona2(c3);

        System.out.println(array2);
    }
}
