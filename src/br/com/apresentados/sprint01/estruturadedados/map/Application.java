package br.com.apresentados.sprint01.estruturadedados.map;

import java.util.LinkedHashMap;
import java.util.Map;

public class Application {

    public static void main(String[] args) {
        long startTime;
        long endTime;
        // Todos herdam de collection.

        //HashMap - LinkedHashMap - TreeMap

        Map<String, String> dicionario = new LinkedHashMap<>();

        startTime = System.nanoTime();
        dicionario.put("Bravura", "Alguém com garra");
        dicionario.put("Inteligente", "Alguém com inteligência");
        dicionario.put("Teste", "Alguma coisa em procedimentos");
        dicionario.put("Coragem", "Alguém que enfrenta medos e desafios");
        endTime = System.nanoTime();
//        for(String word: dicionario.keySet()){
//            System.out.println(word);
//        }

        for (Map.Entry<String, String> entry: dicionario.entrySet()){
            System.out.println("Palavra: "+ entry.getKey() + " Significado: " +  entry.getValue());
        }
        System.out.println("O tempo total de execução é: " + (endTime - startTime));

        startTime = System.nanoTime();
        dicionario.put("Auxílio", "Teste");
        endTime = System.nanoTime();
        System.out.println("O tempo de execução para adicionar é: " + (endTime - startTime));

        startTime = System.nanoTime();
        dicionario.remove("Auxílio");
        endTime = System.nanoTime();
        System.out.println("O tempo de execução para remover é: " + (endTime - startTime));

        startTime = System.nanoTime();
        dicionario.get("Inteligente");
        endTime = System.nanoTime();
        System.out.println("O tempo de execução para um get é de: " + (endTime - startTime));

        System.out.println(dicionario.values());
    }
}
