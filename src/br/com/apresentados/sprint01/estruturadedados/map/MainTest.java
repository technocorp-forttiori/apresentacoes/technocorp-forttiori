package br.com.apresentados.sprint01.estruturadedados.map;

import java.util.HashMap;
import java.util.Map;

public class MainTest {

    public static void main(String[] args) {
        Map<String, String> map = new HashMap<>();
        map.put("primeiroNome", "Matheus");
        map.put("segundoNome", "Otto");
        map.put("empresa", "Technocorp");
        map.put("funcao", "Estagiário");

        for (String key : map.keySet()) {
            System.out.printf("key: %s%n", key);
        }

        for (Map.Entry<String, String> entry : map.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            System.out.printf("key: %s | value: %s%n", key, value);
        }

        map.forEach((k, v) -> System.out.printf("key: %s | value: %s%n", k, v));
    }
}