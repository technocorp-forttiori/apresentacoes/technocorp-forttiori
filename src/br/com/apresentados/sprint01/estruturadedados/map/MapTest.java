package br.com.apresentados.sprint01.estruturadedados.map;

import java.util.HashMap;
import java.util.Map;

public class MapTest {
    public static void main(String[] args) {
        // HashMap Não pode existir chaves duplicadas!
        // Não tem ordenação na hora da impressão.
        // Caso queira manter a ordem de inserção, usar para LinkedHashMap
        Map<String, String> map = new HashMap<>();

        map.put("1","teclado");
        map.put("2","mouse");
        map.put("3","você");

        for(String key : map.values()){
            System.out.println("Valores: " + key);
        }
        for (Map.Entry<String, String> entry: map.entrySet()){
            System.out.println("Chaves e Valores: " + entry.getKey() + " " + entry.getValue());
        }
    }
}
