package br.com.apresentados.sprint01.estruturadedados.equalshashcode;

public class Funcionario {
    private String nome;
    private String cpf;
    private String cargo;

    public Funcionario() {
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }

    @Override
    public String toString() {
        return "{" +
                "nome='" + nome + '\'' +
                ", cpf='" + cpf + '\'' +
                ", cargo='" + cargo + '\'' +
                '}';
    }

//    @Override
//    public boolean equals(Object o) {
//        if (this == o) return true;
//        if (!(o instanceof Funcionario)) return false;
//        Funcionario that = (Funcionario) o;
//        return Objects.equals(cpf, that.cpf);
//    }
//
//    @Override
//    public int hashCode() {
//        return Objects.hash(cpf);
//    }
}
