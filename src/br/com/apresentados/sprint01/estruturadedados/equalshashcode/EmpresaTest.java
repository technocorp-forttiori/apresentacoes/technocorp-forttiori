package br.com.apresentados.sprint01.estruturadedados.equalshashcode;

public class EmpresaTest {
    public static void main(String[] args) {
        Funcionario f1 = new Funcionario();
        f1.setNome("Matheus");
        f1.setCpf("050450454");
        f1.setCargo("Developer");

        Empresa technocorp = new Empresa();
        technocorp.registraFuncionario(f1);

        Funcionario funcionarioBusca = new Funcionario();
        funcionarioBusca.setNome("Matheus");
        funcionarioBusca.setCpf("050450454");
        funcionarioBusca.setCargo("Developer");

        // Não funciona pois pois ele usa o equals para saber se o objeto está dentro.
        // Equals só retorna verdadeiro se eles são iguais, se tem o mesmo lugar no
        // Endereço de Memória.
        // Assim como foi dado funcionarioBusca com new, para o Java o Enderço de Memória
        // Já é outro.

        System.out.println(technocorp.emprega(funcionarioBusca));

        System.out.println("Imprime a lista de funcionários " + '\n' + technocorp.listaFuncionarios() + '\n');
    }
}
