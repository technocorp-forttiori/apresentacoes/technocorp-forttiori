package br.com.apresentados.sprint01.estruturadedados.stacks;

import java.util.Stack;

public class    Stacks {
    public static void main(String[] args) {
        // FIFO
        Stack<String> games = new Stack<>();
        games.add("God of War");
        games.add("kingdom Hearts");
        games.add("Final Fantasy");
        games.add("Resident Evil");

        System.out.println(games);

        // para pegar o item superior da fila - último adicionado
        System.out.println(games.pop());

        // agora que removemos o item do topo, deve retornar apenas os outros 3.
        System.out.println(games);

        // peek espia a pilha, o último item adicionado.
        System.out.println(games.peek());

        // get para verificar por index os itens da pilha por posição

        System.out.println("O primeiro game da pilha é: " + games.get(0));
    }
}
