package br.com.apresentados.sprint01.estruturadedados.array;

import java.util.LinkedList;
import java.util.Queue;

public class ExemploQueue {

    public static void main(String[] args) {
        // Utilizado LinkedList para Implementação de Métodos na criação de Queue(polimorfismo)
        Queue<Integer> fila = new LinkedList<>();
        fila.add(1); //enqueue
        fila.add(2);

        System.out.println(fila);

        System.out.println(fila.peek()); //espiar

        System.out.println(fila.remove()); // dequeue

        System.out.println(fila);

    }
}
