package br.com.apresentados.sprint01.estruturadedados.array;

import java.util.Stack;

public class    ExemploStack {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();

        System.out.println(stack.isEmpty());

        stack.push(1);
        stack.push(2);
        stack.push(3);
        stack.push(4);
        stack.push(5);

        // verificar tamanho
        System.out.println("tamanho da pilha: " + stack.size());
        // imprimindo pilha
        System.out.println("impressão pilha: " + stack);
        // removendo item da pilha
        System.out.println("removendo ultimo item da pilha: " + stack.pop());
        System.out.println(stack);
    }
}
