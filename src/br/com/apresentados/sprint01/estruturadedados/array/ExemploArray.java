package br.com.apresentados.sprint01.estruturadedados.array;

public class ExemploArray {
    public static void main(String[] args) {
        double[] temperaturas = new double[6];
        temperaturas[0] = 31.3;
        temperaturas[1] = 30;
        temperaturas[2] = 35.8;
        temperaturas[3] = 33.7;
        temperaturas[4] = 31.2;
        temperaturas[5] = 31.9;

        System.out.println("O Valor da temperatura do dia 3 é: " + temperaturas[2]);
        System.out.println("O tamanho do array é: " + temperaturas.length);

        for (int i = 0; i < temperaturas.length; i++){
            System.out.println("O valor da temperatura do dia " + (i +1) + " é " +
                    temperaturas[i]);
        }
        for (double temp: temperaturas){
            System.out.println("O valor da temperatura é: " + temp);
        }
    }
}
