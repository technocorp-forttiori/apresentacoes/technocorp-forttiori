package br.com.apresentados.sprint01.estruturadedados.array;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class ExemploList {
    public static void main(String[] args) {
        // aceita valor duplicado
        List<String> nomes = new LinkedList<>();
        nomes.add("Ana");
        nomes.add("Luis");
        nomes.add("Matheus");
        nomes.add("Ana");

        System.out.println(nomes.size());

       // pegando da lista através de uma posição
       String nome = nomes.get(0);
       System.out.println(nome);

        //remover um nome da lista
        //nomes.remove("Ana");

        // verificar se existe na lista
        boolean existeAna = nomes.contains("Ana");
        System.out.println(existeAna);


//        for (String nome : nomes) {
//            System.out.println(nome);
//        }

        // adicionando na lista com posição
        nomes.add(0,"Guilherme");
        System.out.println(nomes);

        // Imprimir lista de forma ordenada
        Collections.sort(nomes);

        System.out.println(nomes);
    }
}