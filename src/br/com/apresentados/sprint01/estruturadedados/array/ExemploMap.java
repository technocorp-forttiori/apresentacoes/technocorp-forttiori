package br.com.apresentados.sprint01.estruturadedados.array;

import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class ExemploMap {

    public static void main(String[] args) {
        // não aceita elemento duplicado, faz uma atualização.
        // HashMap não garante a ordem de impressão, TreeMap garante ordem de impressão
        Map<String, Double> cuponsDeDesconto = new TreeMap<>();
        cuponsDeDesconto.put("CP1" ,10.50);
        cuponsDeDesconto.put("CP2" ,7.50);
        cuponsDeDesconto.put("CP3" ,9.50);
        cuponsDeDesconto.put("CP4" ,12.50);
        cuponsDeDesconto.put("CP5" ,15.50);

//        cuponsDeDesconto.get("CP1");
        //verificar  a partir do Nome
        Double valorDoCupom = cuponsDeDesconto.get("CP1");
        //verifica se existe
        boolean verificarSeCupomExiste = cuponsDeDesconto.containsKey("CP1");

        System.out.println(valorDoCupom);
        System.out.println(verificarSeCupomExiste);

        //Imprimir as chaves
        Set<String> chaves = cuponsDeDesconto.keySet();
        System.out.println(chaves);

        // Imprimir com valor
        for (Map.Entry<String, Double> entrada : cuponsDeDesconto.entrySet()){
            System.out.println(entrada);
        }
    }
}
