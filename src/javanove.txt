JAVA 9

Sistema de módulos é a principal adição do java 9
Conhecido como JigSaw

---- Factory Methods para List, Set e Map

São métodos estáticos que servem para construir objetos
exemplo:
Optional<Object> empty = Optional.empty().
o método empty é um static metod factory, pois ele cria uma nova instancia e retorna algo
== Vantagens
- Nomes com siginificados,
ex: new Optinal - eu não tenho um nome siginfiicativo
já optinal.empty eu sei o que ele vai fazer.

- Flexibilidade no Retorno
Exemplo
ArrayList<Object> list = new ArrayList<>();
List<Object> novaLista  = Collections.unmodifiableList(list);
// Essa lista não é modificável, não dá para remover ou adicionar
Quando eu faço new arrayList (comum) - só posso retornar um new arrayList, só aceita retornos do mesmo TIPO
Porém quando usa static method factory - eu posso retornar outro TIPO, quando utilizado Optional
- Controle das Instâncias
ex: Integer cem = Integer.valueOf(100);
ir até a classe valueOf (ctrl Click)
O integer cem foi criado dentro da classe, ele tem um controle do que foi criado
Porque faz uma verificação, vê se o numero passado é menor ou maior que um numero de valor qualquer( que já tem em cache)
Se tiver entre esses, ele utiliza esse numero em cache, se não tiver, ele cria uma nova instancia desse numero.
outro exemplo é o Optional.empty, sempre retorna o mesmo empty.
um private static final empty, sempre a mesma instância.


---- Wrapper Classes - Construtores obsoletos
CLasses Integer, Double, Booolean etc.
Intenção dessa criação é para estimular o uso dos metodos
parse*() e valueOf().

Exemplo
Antigo:
Integer i1 = new Integer(100);
Integer i2 = new Integer("100");
Novo:
Integer i3 = Integer.valueOf(100);
Integer i4 = Integer.valueOf("100");

int i5 = Integer.parseInt("100");

----------- Métodos Privados em interfaces

default void default1(){
// implementação
    comportamentoCompartilhado();
}
default void default2(){
// implementação
    comportamentoCompartilhado();
}
private void comportamentoCompartilhado(){
// implementação

}


--- Strings ocupam menos espaços na memória.
Antigamente ocupavam 2 bytes
agora ocupam somente 1 e + 1 byte para encoding

Suport a Unicode 8.0 em strings e "afins" emojis ... etc.

Arquivos .properties são finalmente UTF-8 antigamente eram por padrão Encoding latin1 ou iso8859-1
tinha que ir na ide colocar esse encode.


--------- Agora vem a novidade do Java 9 , que são a criação de Methods List,Set e Map.

Como era antes...
       // Como era Antes ---
        List<Integer> list2 = Arrays.asList(cem,duz,tre);
        //Formato Novo Java 9
        // List.of tem vários argumentos.
        List<Integer> of4 = List.of();
        List<Object> of = List.of(cem,100,tre); // Posso criar elementos na lista
        List<Integer> of2 = List.of(cem);
        List<Integer> of3 = List.of(1,2,3);

        E porque usar esse tipo de lista? E não o arraylist;
        Porque essas listas são imutáveis.
        Não há como adicionar ou remover e alterar a lista após ela ser instanciada.

-- String menores. Ver o comparativo das imagens, nas minhas imagens.


---------- Nova Sintaxe Try-With-Resources

