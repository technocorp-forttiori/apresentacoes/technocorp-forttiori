A principal diferença é que a classe abstrata pode ter código e a Interface não.

A Interface é utiliza quando é necessário que uma ou várias classes precisam e devem ter
a mesma assinatura. Exemplo se todos os elementos das classes tem que ter uma assinatura padrão ou protocolos.
Ela garante o contrato/assinatura das classes.

A classe abstrata deve ser usada quando for aplicar em padrões de projeto, boas práticas e reutilização de código.

Sempre que for contrato utilizar uma interface.
Se for utilizar uma abstração utilizar uma classe abstrata.