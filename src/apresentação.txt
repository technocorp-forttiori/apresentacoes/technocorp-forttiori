############ JAVA 8

Tópico 1 - Interfaces Funcionais ?
O que são?
Functional Interfaces são todas as interfaces que possuem
um método abstrato. Isso quer dizer que várias interfaces que já existiam e que atendiam a essa premissa,
automaticamente se tornaram interfaces funcionais.
Functional Interfaces também podem conter um ou mais Default Methods.

É reconhecido como @FunctionalInterface no java.

Mas ... Se o compilador reconhece as interfaces, por que existe uma anotação para isso?

Essa anotação existe para que o desenvolvedor possa forçar um erro de compilação,
caso a interface não atenda os requisitos de uma Functional Interface. Que são:
Ser uma interface e não outro tipo de objeto.
Conter apenas um método abstrato.

Algumas das interfaces funcionais são.
-- Suppiler -> Representa uma função que vai entregar alguma coisa, não recebe nenhum parâmetro.
-- Consumer -> Consumidor, oposto do supplier, não retorna nada, mas recebe um valor. Ex ForEach
-- BiConsumer recebe dois valores
-- Predicate -> É uma função que recebe um valor e retorna um boolean. Basicamente uma comparação.
-- BiPredicate - Recebe Dois valores.
-- Function -> Recebe um valor e retorna um valor.
-- BiFunction - Recebe 2 valores e retorna um novo valor.
-- BinaryOperator - Recebe dois valores e os valores tem que ser do mesmo tipo.

######## GENERICS
O que é generics? E pra que foi criado?
É uma classe, metodo, atributo ou interface que pode receber qualquer tipo, exemplo String, int double etc.
Serve para definir um tipo a ser trabalhado.

######## WildCards

Em Java é um tipo especial de argumento de tipo que controla a segurança de tipo do uso de tipos
genéricos. Pode ser usado em declarações e instanciações de variáveis, bem como em definições de métodos,
....mas não na definição de um tipo genérico.

Temos 3 tipos de WildCards

UNKNOW WILDCARD -> SIGNIFICA QUE PODEMOS RECEBER UM OBJETO DA CLASSE GENÉRICA ESPECIFICADO PRA QUALQUER TIPO DE OBJETO SEM NENHUMA RESTRIÇÃO, EX:
SE FOSSE RECEBER UMA LISTA, PODERIA RECEBER UMA LISTA DE QUALQUER TIPO SEM NENHUMA RESTRIÇÃO. PARA USAR < ? > DEPOIS DO TIPO DA VARIÁVEL

EXTENDS WILDCARD -> PERMITE QUE RECEBA UM OBJETO DA CLASSE GENÉRICA, ESPEFICIADO PARA UMA CLASSE DEFINIDA OU SEUS DESCENDENTES, FILHOS ETC.
PARA USAR < ? EXTENDS CLASSE >

SUPER WILDCARD -> Permite que receba um objeto da classe genérica especificado para a classe definida ou seus ascendentes.
Classe Pai, classe avós, etc.

####### STREAMS
Considerando uma lista de Clientes List<Cliente> clientes podemos usar Streams para executar
várias tarefas que antes precisavam de muito código e que poderiam ser escritas de maneiras
distintas. Com Streams essas tarefas ficam mais simples, estruturadas e padronizadas.
Algumas funcionalidades desta API.

count - Retorna a quantidade de elementos presentes em uma stream. Mas se você tem uma lista prefira clientes.size().
limit - Retorna uma nova stream que contém apenas os N primeiros elementos da stream original.
skip - Retorna uma nova stream que não contém os N primeiros elementos da stream original.
sorted - Retorna uma nova stream contendo os elementos da stream original ordenados pela forma natural em ordem crescente.
sorted.reversed - Retorna uma nova stream contendo os elementos da stream original ordenados de acordo com algum critério em ordem decrescente.
filter - Filtra os elementos de acordo com uma condição retornando uma nova stream.
map - Retorna uma stream consistindo no resultado da aplicação de uma função de mapeamento nos elementos da stream.
reduce - Realiza uma operação de redução que leva uma sequência de elementos de entrada e os combina em um único resultado, como acumular valores.
forEach - Realiza uma iteração sobre todos os elementos de uma stream e executa algum tipo de processamento. É equivalente ao loop for (Cliente cliente: clientes).
collect - Permite coletar o conteúdo da stream, por exemplo como uma lista.

-- Diferença Reduce para Collect

O reduce é para trabalhar com valores imutaveis
Collect é para para trabalhar com valores mutáveis.

########## Lambda

Uma expressão Lambda é uma expressão que permite definir uma interface funcional (novamente, um método abstrato)
que o compilador identifica pela estrutura. O compilador pode determinar a interface funcional
representada a partir de sua posição. O tipo de uma expressão lambda é o da interface funcional associada

Como funciona?

O formato da expressão é.
A primeira parte são os argumentos que a expressão recebe
Exemplo : jButton2.addActionListener("e"(<- argumento) -> (Implementação do método) System.out.println("Olá Mundo!");

Mas não foi feita as interfaces funcionais somente para clean code ou, visualização melhor do código.
Foi lançado mais para fazer as API de STREAMS

Mas o que é uma stream?
É basicamente um Fluxo de dados

Stream = loops implícitos
Ele é controlado pela própria IDE, recebe os dados e faz as funções que é ordenado fazer da forma que o stream achar melhor
Tem a opção de utilizar o parallelStream.

for - while - do..while loops explícitos.
o desenvolver controla o loop, o que geralmente é pior...
pois tem que ter os controles, se é maior ou menor, se é igual a algo ou não etc,
ou seja sem usar stream o código fica mais complexo.


Outras formas de criar stream
De um array utilizando Arrays.stream(Object[]);
De métodos estáticos em classes de stream, como Stream.of(Object[]), IntStream.range(int, int);
As linhas de um arquivo podem ser obtidas com BufferedReader.lines();
Streams de arquivos podem ser obtidos através de métodos na classe Files;
Streams de números aleatórios podem ser obtidos em Random.ints().


################ OPTIONAL

A principal proposta deste recurso é encapsular o retorno de métodos e informar se um valor do tipo <T> está presente ou ausente.
Quando você pode ter um resultado ou não, a forma normal do Java sempre foi dar uma exceção quando não der o resultado.

Usando o Optional é possível:
Evitar erros NullPointerException.
Parar de fazer verificações de valor nulo do tipo if (cliente != null).
Escrever código mais limpo e elegante.

É ótima para trabalhar com valores que podem ser Null

É possível também trabalhar com tipos primitivos.
Option foi feito para ser usado como retornos de métodos.
Não é de boa prática receber Option como argumentos.

Exemplo public static OptionalInt retornaAlgo(Optional<String>){}

  /**
     * isPresent
     * <p>
     * é o método para verificar se a string está sendo preenchido ou não
     * <p>
     * get()
     * <p>
     * é o método para chamar o que está sendo passado
     * lembrando que o get é a forma mais simples de buscar o valor dentro do optional
     * <p>
     * ifPresent()
     * <p>
     * recebe uma expressão lambda, ele só executa a expressão lambda se tiver algum valor dentro do Optional
     * se passar algo que pode ser convertido, ele retorna o próprio valor
     * <p>
     * orElse()
     * <p>
     * é possível definir um valor default caso o Optional não possa ser convertido
     * <p>
     * orElseGet()
     * <p>
     * mesma lógica do orElse(), mas também recebe uma função lambda
     * <p>
     * orElseThrow
     * <p>
     * também carrega a mesma lógica do orElse, mas é possível através dele lançar uma exceção dentro de uma expressão lambda
     */



########### API DE DATAS

Um dos principais conceitos dessa nova API é a separação de como
dados temporais são interpretados em duas categorias: a dos computadores e a dos humanos.

        // LocalDate - Representa uma Data
        // LocalTime - Representa uma hora: 12:00:00
        // LocalDateTime - Representa Data + Hora. 01/01/2000 12:00:00
        // Instant - Rpresenta um instante / momento, na linha do tempo. (milissegundos a partir de 01/01/1970 00:00:00)
        // Instant - Também representa 01/01/2000 12:00:00 GMT/UTC (não tem localidade)

        // ZonedDateTime - LocalDateTime com TimeZone (fuso horário)
        // 06/08/1990 12:00:00 GMT-3 (America/BRASILIA)
        // A diferença entre Instant e ZonedDatTime, é que Instant não armazena informação do fuso horário.


-- Period -> Armazena um espaço de tempo. Trabalha com datas valores baseados em datas, 1 dia semanas etc.
-- Duration -> trabalha com duração em tempo, minutos, segundos etc.



################# JAVA 9
Sistema de módulos é a principal adição do java 9
Implementar aplicações de uma maneira modular estimula boas práticas de design, como a separação de responsabilidades e encapsulamento.
O Sistema de Módulos da Plataforma Java (JPMS) permite aos desenvolvedores definir quais são os módulos da aplicação,
como eles devem ser usados por outros módulos, e de quais outros módulos eles dependem.
É possível adicionar o JPMS em aplicações que já estão utilizando um sistema diferente para definir os módulos da aplicação,
por exemplo: módulos Maven ou subprojetos Gradle.
O JDK traz ferramentas para ajudar os desenvolvedores a migrar códigos existentes para o JPMS.
Aplicações ainda podem ter dependências de bibliotecas construídas em versões anteriores ao Java 9. Esses arquivos jar
são tratados como um módulo "automático" especial, o que facilita a migração gradual para o Java 9.

O sistema de módulos tbm é conhecido como JigSaw


#### Factory Methods para List, Set e Map

São métodos estáticos que servem para construir objetos
exemplo:
Optional<Object> empty = Optional.empty().
o método empty é um static metod factory, pois ele cria uma nova instancia e retorna algo
== Vantagens
- Nomes com siginificados,
ex: new Optinal - eu não tenho um nome siginfiicativo
já optinal.empty eu sei o que ele vai fazer.

- Flexibilidade no Retorno
Exemplo
ArrayList<Object> list = new ArrayList<>();
List<Object> novaLista  = Collections.unmodifiableList(list);
// Essa lista não é modificável, não dá para remover ou adicionar
Quando eu faço new arrayList (comum) - só posso retornar um new arrayList, só aceita retornos do mesmo TIPO
Porém quando usa static method factory - eu posso retornar outro TIPO, quando utilizado Optional
- Controle das Instâncias
ex: Integer cem = Integer.valueOf(100);
ir até a classe valueOf (ctrl Click)
O integer cem foi criado dentro da classe, ele tem um controle do que foi criado
Porque faz uma verificação, vê se o numero passado é menor ou maior que um numero de valor qualquer( que já tem em cache)
Se tiver entre esses, ele utiliza esse numero em cache, se não tiver, ele cria uma nova instancia desse numero.
outro exemplo é o Optional.empty, sempre retorna o mesmo empty.
um private static final empty, sempre a mesma instância.


---- Wrapper Classes - Construtores obsoletos
CLasses Integer, Double, Booolean etc.
Intenção dessa criação é para estimular o uso dos metodos
parse*() e valueOf().

Exemplo
Antigo:
Integer i1 = new Integer(100);
Integer i2 = new Integer("100");
Novo:
Integer i3 = Integer.valueOf(100);
Integer i4 = Integer.valueOf("100");

int i5 = Integer.parseInt("100");

----------- Métodos Privados em interfaces

default void default1(){
// implementação
    comportamentoCompartilhado();
}
default void default2(){
// implementação
    comportamentoCompartilhado();
}
private void comportamentoCompartilhado(){
// implementação

}


--- Strings ocupam menos espaços na memória.
Antigamente ocupavam 2 bytes
agora ocupam somente 1 e + 1 byte para encoding

Suport a Unicode 8.0 em strings e "afins" emojis ... etc.

Arquivos .properties são finalmente UTF-8 antigamente eram por padrão Encoding latin1 ou iso8859-1
tinha que ir na ide colocar esse encode.


--------- Agora vem a novidade do Java 9 , que são a criação de Methods List,Set e Map.

Como era antes...
       // Como era Antes ---
        List<Integer> list2 = Arrays.asList(cem,duz,tre);
        //Formato Novo Java 9
        // List.of tem vários argumentos.
        List<Integer> of4 = List.of();
        List<Object> of = List.of(cem,100,tre); // Posso criar elementos na lista
        List<Integer> of2 = List.of(cem);
        List<Integer> of3 = List.of(1,2,3);

        E porque usar esse tipo de lista? E não o arraylist;
        Porque essas listas são imutáveis.
        Não há como adicionar ou remover e alterar a lista após ela ser instanciada.

-- String menores. Ver o comparativo das imagens, nas minhas imagens.


---------- Nova Sintaxe Try-With-Resources

############ JAVA 10

--- NOVAS FUNCIONALIDADES JAVA 10

MUITO ALÉM DO "VAR" É UMA DAS GRANDES MUDANÇAS DO JAVA 10
VAR - CONTAINER - IMUTABILIDADE

VAR - UMA FORMA DE FAZER DECLARAÇÃO DE VARIVEIS MAIS ENXUTA

Objetivo é reduzir uso de repetição de código. que é chamado de boilerplatecode;
ex: de : ex: ByteArrayInputStream bais = ByteArrayInputStream(null);
para: var bais = ByteArrayInputStream(null);

VAR NÃO É UMA PALAVRA RESERVADA.
EX: var var = 50;
Ex: não posso criar =  public class var{} pois é uma palavra de tipo.
Var var = new Var()

var é chamado de reserved type name.


-- Novos métodos para copiar coleções.
List - copyOf(Collection)
Map - copyOf(Map)
Set - copyOf(Collection)

-- Novos Collectors para Streams
toUnmodifiableList()
toUnmodifiableMap(Function,Function)
toUnmodifiableMap(Function,Function,...)
toUnmodifiableSet()

-- Alternativa ao Optional.get()
OptionalDouble -> orElseThrow()
OptionalLong -> orElseThrow()
OptionalInt -> orElseThrow()
Optional -> orElseThrow()

-- Geração de Bytecode otimizada para for-each

-- Novas language tags em Locale.
